export interface Deserializable<T> {
	deserialize(input: any) : T
}

export class Document<T> implements Deserializable<T> {
	_id: any;
	public deserialize(input) : T {
		this._id = input._id;
		return null;
	}
}

export class NestedDocument implements Deserializable<any> {
	id: number;
	public deserialize(input) {
		this.id = input.id && parseInt(input.id);
		return null;
	}
}

// Вспомогательный класс
export class DocumentHelper {
	static initIds(docs: NestedDocument[]){
		let i=1;
		docs.forEach(d => d.id = i++);
	}
	static getNextId(docs: {id: number}[]) : number {
		if (!docs || docs.length == 0) return 1;
		let max = docs[0].id;
		docs.forEach(d => max = d.id > max ? d.id : max);
		return max + 1;
	}
}



class SingleDocument extends Document<SingleDocument> {
	type: string;
	constructor(type: string) {
		super();
		this.type = type;
	}
	public deserialize(input) : SingleDocument {
		super.deserialize(input);
		return this;
	}
}

export class Settings extends SingleDocument {
	companyName: string;
	phone: string;
	address: string;
	email: string;
	mapX: number;
	mapY: number;
	description: string;
	headerImageUrl: string;
	mainPageText: string;
	mainPageTitle: string;

	constructor(){
		super('Settings');
	}

	public deserialize(input: any) : Settings {
		if (!input) return this;
		
		super.deserialize(input);

		this.companyName = input.companyName;
		this.phone = input.phone;
		this.address = input.address;
		this.email = input.email;
		this.mapX = parseFloat(input.mapX) || 0;
		this.mapY = parseFloat(input.mapY) || 0;
		this.description = input.description;
		this.headerImageUrl = input.headerImageUrl;
		this.mainPageText = input.mainPageText;
		this.mainPageTitle = input.mainPageTitle;

		return this;
	}
}

export class Review extends Document<Review> {
	author: string;
	photoUrl: string;
	comment: string;
	date: Date;
	published: boolean;

	public deserialize(input: any) : Review {
		super.deserialize(input);
		this.author = input.author;
		this.photoUrl = input.photoUrl;
		this.comment = input.comment;
		this.date = new Date(input.date);
		this.published = input.published;
		return this;
	} 
}

export class ClientRequest {
	fio: string;
	email: string;
	phone: string;
	comment: string;
	public deserialize(input: any) : ClientRequest {
		this.fio = input.fio;
		this.email = input.email;
		this.phone = input.phone;
		this.comment = input.comment;
		return this;
	}
}

export class Renova extends Document<Renova> {
	code: string;
	call2Action: string;
	headerImageUrl: string;
	menuText: string;
	text: string;
	published: boolean;
	public deserialize(input: any) : Renova {
		super.deserialize(input);
		this.code = input.code;
		this.call2Action = input.call2Action;
		this.headerImageUrl = input.headerImageUrl;
		this.menuText = input.menuText;
		this.text = input.text;
		this.published = input.published;
		return this;
	}
}

export class PriceList extends Document<PriceList> {
	constructor() {
		super();
		this.groups = [];
	}
	code: string;
	name: string;
	priceChangeOnly: boolean;
	published: boolean;
	groups: PriceGroup[];
	comment: string;
	deserialize(input: any) : PriceList {
		super.deserialize(input);
		this.code = input.code;
		this.name = input.name;
		this.groups = input.groups && input.groups.map(g => new PriceGroup().deserialize(g)) || [];
		this.priceChangeOnly = input.priceChangeOnly;
		this.published = input.published;
		this.comment = input.comment; 
		return this;
	}
}

export class PriceGroup extends NestedDocument {
	constructor() {
		super();
		this.prices = [];
	}
	name: string;
	prices: Price[];
	deserialize(input: any) : PriceGroup {
		super.deserialize(input);
		this.name = input.name;
		this.prices = input.prices && input.prices.map(p => new Price().deserialize(p)) || [];
		return this;
	}
}

export class Price extends NestedDocument {
	name: string;
	value: number;
	unitId: number;
	deserialize(input: any) : Price {
		super.deserialize(input);
		this.name = input.name;
		this.value = input.value && parseFloat(input.value);
		this.unitId = input.unitId && parseInt(input.unitId);
		return this;
	}
}

interface Unit {
	id: number;
	name: string;
}

// В коде есть ссылки на Units по названию

export const Units: Unit[] = [
	{ id: 0, name: 'м2' },
	{ id: 1, name: 'м п' },
	{ id: 2, name: 'шт' },
	{ id: 3, name: 'точка' },
	{ id: 4, name: 'пог. м' },
	{ id: 5, name: 'м3' },
	{ id: 6, name: 'вых' },
];

export class Calculator extends Document<Calculator> {
	constructor() {
		super();
		this.items = [];
		this.schemas = [];
	}
	priceListId: string;
	items: CalculatorItem[];
	schemas: CalculateSchema[];
	deserialize(input: any) : Calculator {
		if (!input) return this;
		
		super.deserialize(input);
		this.priceListId = input.priceListId;
		this.items = input.items && input.items.map(i => new CalculatorItem().deserialize(i)) || [];
		this.schemas = input.schemas && input.schemas.map(s => new CalculateSchema().deserialize(s)) || [];
		return this;
	}
}

export class CalculatorItem extends NestedDocument {
	groupId: number;
	priceId: number;
	deserialize(input: any) : CalculatorItem {
		super.deserialize(input);
		this.groupId = input.groupId && parseInt(input.groupId);
		this.priceId = input.priceId && parseInt(input.priceId);
		return this;
	}
}

export class CalculateSchema extends NestedDocument {
	constructor() {
		super();
		this.items = [];
	}
	name: string;
	published: boolean;
	items: CalculateSchemaItem[];
	deserialize(input: any) : CalculateSchema {
		super.deserialize(input);
		this.name = input.name;
		this.published = input.published;
		this.items = input.items && input.items.map(i => new CalculateSchemaItem().deserialize(i)) || [];
		return this;
	}
}

export class CalculateSchemaItem extends CalculatorItem {
	quantity: number;
	deserialize(input: any) : CalculateSchemaItem {
		super.deserialize(input);
		this.quantity = input.quantity && parseInt(input.quantity);
		return this;
	}
}


export class Article extends Document<Article> {
	code: string;
	imageUrl: string;
	title: string;
	annotation: string;
	text: string;
	published: boolean;
	date: Date;
	deserialize(input: any) : Article {
		super.deserialize(input);
		this.code = input.code;
		this.imageUrl = input.imageUrl;
		this.title = input.title;
		this.annotation = input.annotation;
		this.text = input.text;
		this.published = input.published;
		this.date = new Date(input.date);
		return this;
	}
}

export class PortfolioWork extends Document<PortfolioWork> {
	constructor() {
		super();
		this.photos = [];
	}
	code: string;
	renovaId: string;
	title: string;
	annotation: string;
	text: string;
	date: Date;
	mainPhoto: string;
	photos: string[];
	published: boolean;
	deserialize(input: any) : PortfolioWork {
		super.deserialize(input);
		this.code = input.code;
		this.renovaId = input.renovaId;
		this.title = input.title;
		this.annotation = input.annotation;
		this.text = input.text;
		this.date = new Date(input.date);
		this.mainPhoto = input.mainPhoto;
		this.photos = input.photos;
		this.published = input.published;
		return this;
	}
}

export class Video extends Document<Video> {
	title: string;
	annotation: string;
	date: Date;
	url: string;
	published: boolean;
	deserialize(input: any) : Video {
		super.deserialize(input);
		this.title = input.title;
		this.annotation = input.annotation;
		this.date = new Date(input.date);
		this.url = input.url;
		this.published = input.published;
		return this;
	}
}

export class SearchResult {
	reviews: Review[];
	renovas: Renova[];
	priceLists: PriceList[];
	priceGroups: PriceGroup[];
	prices: Price[];
	articles: Article[];
	works: PortfolioWork[];
	videos: Video[];

	constructor() {
		this.reviews = [];
		this.renovas = [];
		this.priceGroups = [];
		this.priceLists = [];
		this.prices = [];
		this.articles = [];
		this.works = [];
		this.videos = [];
	}
}