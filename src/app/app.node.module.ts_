// Fix Material Support
import { __platform_browser_private__ } from '@angular/platform-browser';
function universalMaterialSupports(eventName: string): boolean { return Boolean(this.isCustomEvent(eventName)); }
__platform_browser_private__.HammerGesturesPlugin.prototype.supports = universalMaterialSupports;
// End Fix Material Support

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UniversalModule, isBrowser, isNode } from 'angular2-universal/node'; // for AoT we need to manually split universal packages
import { COMPILER_PROVIDERS } from '@angular/compiler';

// Application
import { AppComponent } from './app.component';
import { AdminModule } from './admin/admin.module';
import { SharedModule } from './shared/shared.module';
import { APP_DIRECTIVES, APP_SERVICES } from '.';
import { routing } from './app.routing';

import { StoreModule } from '@ngrx/store';
import { combinedReducers } from './store';

import { provideYmapsNode } from './home/ymaps';
import { provideTextEditorNodeService } from './admin';

import { Cache } from './universal-cache';

@NgModule({
	bootstrap: [ AppComponent ],
	declarations: [ AppComponent,  ...APP_DIRECTIVES ],
	imports: [
		UniversalModule, // NodeModule, NodeHttpModule, and NodeJsonpModule are included
		routing,
		AdminModule,
		SharedModule,
		StoreModule.provideStore(combinedReducers),
	],
	providers: [
		COMPILER_PROVIDERS,
		provideYmapsNode(),
		provideTextEditorNodeService(),
		APP_SERVICES,
		{ provide: 'isBrowser', useValue: isBrowser },
		{ provide: 'isNode', useValue: isNode },
		Cache
	]
})
export class MainModule {
	constructor(public cache: Cache) {
			
	}
	// we need to use the arrow function here to bind the context as this is a gotcha in Universal for now until it's fixed
	universalDoDehydrate = (universalCache) => {
		universalCache['Cache'] = JSON.stringify(this.cache.dehydrate());
	}
	universalAfterDehydrate = () => {
		this.cache.clear();
	}
}