import { reviewsReducer } from './reducers/reviews.reducer'; 
import { settingsReducer } from './reducers/settings.reducer'; 
import { renovasReducer } from './reducers/renovas.reducer';
import { priceListsReducer } from './reducers/priceLists.reducer';
import { calculatorReducer } from './reducers/calculator.reducer';
import { homeStateReducer } from './reducers/homeState.reducer';
import { articlesReducer } from './reducers/articles.reducer';
import { portfolioWorksReducer } from './reducers/portfolioWorks.reducer';
import { videosReducer } from './reducers/videos.reducer';
import { searchReducer } from './reducers/search.reducer';

import { combineReducers } from '@ngrx/store';
import { compose } from '@ngrx/core/compose';

const reducers = {
	reviews: reviewsReducer, 
	settings: settingsReducer, 
	renovas: renovasReducer,
	priceLists: priceListsReducer,
	calculator: calculatorReducer,
	homeState: homeStateReducer,
	articles: articlesReducer,
	portfolioWorks: portfolioWorksReducer,
	videos: videosReducer,
	search: searchReducer
}

const productionReducer = combineReducers(reducers);

export function reducer(state: any, action: any) {
 	return productionReducer(state, action);
}

export * from './reducers/reviews.reducer';
export * from './reducers/settings.reducer';
export * from './reducers/renovas.reducer';
export * from './reducers/priceLists.reducer';
export * from './reducers/calculator.reducer';
export * from './reducers/homeState.reducer';
export * from './reducers/articles.reducer';
export * from './reducers/portfolioWorks.reducer';
export * from './reducers/videos.reducer';
export * from './reducers/search.reducer';
export * from './appState';