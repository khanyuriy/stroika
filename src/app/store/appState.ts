import { SearchResult, Settings, Review, Renova, PriceList, Calculator, Article, PortfolioWork, Video }  from '../../domain';

export const INITIAL_STATE_LOAD = 'INITIAL_STATE_LOAD';

export interface HomeState {
	headerImageUrl: string;
}

export interface SearchState {
	query: string;
	result: SearchResult;
}

export interface AppState {
	settings: Settings;
	calculator: Calculator;
	reviews: Review[];
	renovas: Renova[];
	priceLists: PriceList[];
	articles: Article[];
	homeState: HomeState;
	portfolioWorks: PortfolioWork[];
	videos: Video[];
	search: SearchState;
}