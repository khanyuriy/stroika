import { Action } from '@ngrx/store';
import { INITIAL_STATE_LOAD } from '../appState';
import { PortfolioWork } from '../../../domain';

export const PORTFOLIOWORKS_GET = 'PORTFOLIOWORKS_GET';
export const PORTFOLIOWORKS_POST = 'PORTFOLIOWORKS_POST';
export const PORTFOLIOWORKS_PUT = 'PORTFOLIOWORKS_PUT';
export const PORTFOLIOWORKS_DELETE = 'PORTFOLIOWORKS_DELETE';

export function portfolioWorksReducer(portfolioWorks: PortfolioWork[], action: Action) {
	switch (action.type) {
		case PORTFOLIOWORKS_GET: 
		case INITIAL_STATE_LOAD:
			return action.payload.portfolioWorks.map(r => new PortfolioWork().deserialize(r));
		case PORTFOLIOWORKS_POST: 
			var res = portfolioWorks.map(r => new PortfolioWork().deserialize(r));
			res.unshift(new PortfolioWork().deserialize(action.payload));
			return res;
		case PORTFOLIOWORKS_PUT:
			return portfolioWorks.map(r => {
				if (r._id == action.payload._id) {
					return new PortfolioWork().deserialize(Object.assign({}, r, action.payload));
				}
				return new PortfolioWork().deserialize(r)
			});
		case PORTFOLIOWORKS_DELETE: 
			return portfolioWorks.filter(r => r._id != action.payload._id)
						.map(r => new PortfolioWork().deserialize(r));
		default:
			return portfolioWorks;
	}
}