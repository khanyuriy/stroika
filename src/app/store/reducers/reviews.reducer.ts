import { Action } from '@ngrx/store';
import { AppState } from '../appState';
import { Review } from '../../../domain';
import { INITIAL_STATE_LOAD } from '../appState';

export const REVIEW_LOAD = 'REVIEW_LOAD';
export const REVIEW_POST = 'REVIEW_POST';
export const REVIEW_PUT = 'REVIEW_PUT';
export const REVIEW_DELETE = 'REVIEW_DELETE';

export const reviewsReducer = function(reviews: Review[], action: Action) {
	switch (action.type) {
		case INITIAL_STATE_LOAD: 
			reviews = action.payload.reviews.map((r: Review) => new Review().deserialize(r));
			return reviews;
		case REVIEW_LOAD:
			reviews = action.payload.map((r: Review) => new Review().deserialize(r));
			return reviews;

		case REVIEW_POST: 
			reviews = reviews.map((r: Review) => new Review().deserialize(r));
			reviews.unshift(new Review().deserialize(action.payload));
			return reviews;

		case REVIEW_PUT: 
			reviews = reviews.map((r: Review) => {
				if (r._id == action.payload._id) {
					return new Review().deserialize(Object.assign({}, r, action.payload));
				}
				return new Review().deserialize(r)
			});
			return reviews;

		case REVIEW_DELETE:
			return reviews
				.filter(r => r._id != action.payload._id)
				.map(r => new Review().deserialize(r));

		default:
			return reviews
	}
}