import { Action } from '@ngrx/store';
import { INITIAL_STATE_LOAD } from '../appState';
import { Article } from '../../../domain';

export const ARTICLE_GET = 'ARTICLE_GET';
export const ARTICLE_POST = 'ARTICLE_POST';
export const ARTICLE_PUT = 'ARTICLE_PUT';
export const ARTICLE_DELETE = 'ARTICLE_DELETE';

export function articlesReducer(articles: Article[], action: Action) {
	switch (action.type) {
		case ARTICLE_GET: 
		case INITIAL_STATE_LOAD:
			return action.payload.articles.map(r => new Article().deserialize(r));
		case ARTICLE_POST: 
			var res = articles.map(r => new Article().deserialize(r));
			res.unshift(new Article().deserialize(action.payload));
			return res;
		case ARTICLE_PUT:
			return articles.map(r => {
				if (r._id == action.payload._id) {
					return new Article().deserialize(Object.assign({}, r, action.payload));
				}
				return new Article().deserialize(r)
			});
		case ARTICLE_DELETE: 
			return articles.filter(r => r._id != action.payload._id)
						.map(r => new Article().deserialize(r));
		default:
			return articles;
	}
}