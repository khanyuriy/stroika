import { Action } from '@ngrx/store';
import { INITIAL_STATE_LOAD } from '../appState';
import { Calculator } from '../../../domain';

export const CALCULATOR_LOAD = 'CALCULATOR_LOAD';
export const CALCULATOR_POST = 'CALCULATOR_POST';
export const CALCULATOR_PUT = 'CALCULATOR_PUT';
export const CALCULATOR_DELETE = 'CALCULATOR_DELETE';

export function calculatorReducer(calculator: Calculator, action: Action) {
	switch (action.type) {
		case INITIAL_STATE_LOAD:
			return new Calculator().deserialize(action.payload.calculator);
		case CALCULATOR_LOAD:
			return new Calculator().deserialize(action.payload);
		case CALCULATOR_PUT:
			return new Calculator().deserialize(Object.assign({}, calculator, action.payload));
		default:
			return calculator;
	}
}