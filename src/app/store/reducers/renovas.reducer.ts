import { Action } from '@ngrx/store';
import { INITIAL_STATE_LOAD } from '../appState';
import { Renova } from '../../../domain';

export const RENOVA_POST = 'RENOVA_POST';
export const RENOVA_PUT = 'RENOVA_PUT';
export const RENOVA_DELETE = 'RENOVA_DELETE';

export function renovasReducer(renovas: Renova[], action: Action) {
	switch (action.type) {
		case INITIAL_STATE_LOAD:
			return action.payload.renovas.map(r => new Renova().deserialize(r));
		case RENOVA_POST: 
			var res = renovas.map(r => new Renova().deserialize(r));
			res.unshift(new Renova().deserialize(action.payload));
			return res;
		case RENOVA_PUT:
			return renovas.map(r => {
				if (r._id == action.payload._id) {
					return new Renova().deserialize(Object.assign({}, r, action.payload));
				}
				return new Renova().deserialize(r)
			});
		case RENOVA_DELETE: 
			return renovas.filter(r => r._id != action.payload._id)
						.map(r => new Renova().deserialize(r));
		default:
			return renovas;
	}
}