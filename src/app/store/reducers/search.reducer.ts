import { Action } from '@ngrx/store';
import { SearchState,  } from '../';
import { SearchResult, Article } from '../../../domain';

export var SEARCH_SET_RESULTS = 'SEARCH_SET_RESULTS';

export function searchReducer(state: SearchState, action: Action) {
	switch (action.type) {
		case SEARCH_SET_RESULTS:
			let theState: SearchState = {
				query: action.payload.query,
				result: new SearchResult()
			};
			Object.assign(theState.result, action.payload.result);
			// theState.result.articles = action.payload.result.articles;
			// theState.result.priceGroups = action.payload.result.priceGroups;
			// theState.result.priceLists = action.payload.result.priceLists;
			// theState.result.prices = action.payload.result.prices;
			// theState.result.renovas = action.payload.result.renovas;
			// theState.result.reviews = action.payload.result.reviews;
			// theState.result.videos = action.payload.result.videos;
			// theState.result.works = action.payload.result.works;
			return theState;
		default:
			return state;
	}
}
