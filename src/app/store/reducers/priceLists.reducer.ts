import { Action } from '@ngrx/store';
import { INITIAL_STATE_LOAD } from '../appState';
import { PriceList } from '../../../domain';

export const PRICELIST_POST = 'PRICELIST_POST';
export const PRICELIST_PUT = 'PRICELIST_PUT';
export const PRICELIST_DELETE = 'PRICELIST_DELETE';

export function priceListsReducer(priceLists: PriceList[], action: Action) {
	switch (action.type) {
		case INITIAL_STATE_LOAD:
			return action.payload.priceLists.map(r => new PriceList().deserialize(r));
		case PRICELIST_POST: 
			var res = priceLists.map(r => new PriceList().deserialize(r));
			res.unshift(new PriceList().deserialize(action.payload));
			return res;
		case PRICELIST_PUT:
			return priceLists.map(r => {
				if (r._id == action.payload._id) {
					return new PriceList().deserialize(Object.assign({}, r, action.payload));
				}
				return new PriceList().deserialize(r)
			});
		case PRICELIST_DELETE: 
			return priceLists.filter(r => r._id != action.payload._id)
						.map(r => new PriceList().deserialize(r));
		default:
			return priceLists;
	}
}