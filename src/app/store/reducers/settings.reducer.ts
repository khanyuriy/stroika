import { ActionReducer, Action } from '@ngrx/store';
import { Settings } from '../../../domain';
import { INITIAL_STATE_LOAD } from '../appState';

export const SETTINGS_LOAD = 'SETTINGS_LOAD';
export const SETTINGS_PUT = 'SETTINGS_PUT';

export const settingsReducer : ActionReducer<Settings> = (state: Settings, action: Action) => {
	switch (action.type) {
		case INITIAL_STATE_LOAD: 
			return new Settings().deserialize(action.payload.settings);
		case SETTINGS_LOAD:
			return new Settings().deserialize(action.payload);
		case SETTINGS_PUT:
			var set = new Settings().deserialize(state)
			return Object.assign(set, action.payload);
		default:
			return state;
	}
}