import { Action } from '@ngrx/store';
import { HomeState } from '../appState';
import { Renova } from '../../../domain';
import { INITIAL_STATE_LOAD } from '../../store'

export const HOME_SET_HEADER_IMAGE = 'HOME_SET_HEADER_IMAGE';

export function homeStateReducer(homeState: HomeState, action: Action) {
	switch (action.type) {
		case INITIAL_STATE_LOAD:
			return { headerImageUrl: '' };
		case HOME_SET_HEADER_IMAGE: 
			return { headerImageUrl: action.payload.headerImageUrl };
		default:
			return homeState;
	}
}