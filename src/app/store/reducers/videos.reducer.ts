import { Action } from '@ngrx/store';
import { INITIAL_STATE_LOAD } from '../appState';
import { Video } from '../../../domain';

export const VIDEOS_GET = 'VIDEOS_GET';
export const VIDEOS_POST = 'VIDEOS_POST';
export const VIDEOS_PUT = 'VIDEOS_PUT';
export const VIDEOS_DELETE = 'VIDEOS_DELETE';

export function videosReducer(videos: Video[], action: Action) {
	switch (action.type) {
		case VIDEOS_GET: 
		case INITIAL_STATE_LOAD:
			return action.payload.videos.map(r => new Video().deserialize(r));
		case VIDEOS_POST: 
			var res = videos.map(r => new Video().deserialize(r));
			res.unshift(new Video().deserialize(action.payload));
			return res;
		case VIDEOS_PUT:
			return videos.map(r => {
				if (r._id == action.payload._id) {
					return new Video().deserialize(Object.assign({}, r, action.payload));
				}
				return new Video().deserialize(r)
			});
		case VIDEOS_DELETE: 
			return videos.filter(r => r._id != action.payload._id)
						.map(r => new Video().deserialize(r));
		default:
			return videos;
	}
}