import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { Settings, Review, ClientRequest, Renova, Document, PriceList,
		Calculator, Article, Video, PortfolioWork } from '../domain';
import { Store, Action } from '@ngrx/store';
import * as _ from 'lodash';

import { 
	AppState, INITIAL_STATE_LOAD, 
	SETTINGS_LOAD, SETTINGS_PUT, 
	CALCULATOR_LOAD, CALCULATOR_PUT,
	REVIEW_LOAD, REVIEW_POST, REVIEW_PUT, REVIEW_DELETE,
	RENOVA_POST, RENOVA_PUT, RENOVA_DELETE,
	PRICELIST_POST, PRICELIST_PUT, PRICELIST_DELETE,
	ARTICLE_GET, ARTICLE_POST, ARTICLE_PUT, ARTICLE_DELETE,
	PORTFOLIOWORKS_GET, PORTFOLIOWORKS_POST, PORTFOLIOWORKS_PUT, PORTFOLIOWORKS_DELETE,
	VIDEOS_GET, VIDEOS_POST, VIDEOS_PUT, VIDEOS_DELETE
} from './store';


@Injectable()
export class AppService {

	lastWorks$: Observable<PortfolioWork[]>;
	renovas$: Observable<Renova[]>;
	lastArticles$: Observable<Article[]>;
	lastReviews$: Observable<Review[]>;
	videos$: Observable<Video[]>;
	lastVideos$: Observable<Video[]>;

	constructor(private http: Http, private store: Store<AppState>) {
		this.lastWorks$ = store.select(s => s.portfolioWorks)
			.map(works => this.last3Entities(works));

		this.lastArticles$ = this.store.select(state => state.articles)
			.map(articles => this.last3Entities(articles));

		this.lastReviews$ = this.store.select(state => state.reviews)
			.map(reviews => this.last3Entities(reviews));
		
		this.videos$ = this.store.select(state => state.videos)
			.map(videos => _(videos)
				.filter({published: true})
				.sortBy(video => video.date)
				.reverse()
				.valueOf());
		
		this.lastVideos$ = this.videos$
			.map(videos => _.take(videos, 3));

		this.renovas$ = store.select(s => s.renovas).map(renovas => _.filter(renovas, {published: true}));
	}

	public lastWorksByRenova(renovaId: string) {
		return this.store.select(s => s.portfolioWorks)
			.map(works => _.filter(works, {renovaId: renovaId}))
			.map(works => this.last3Entities(works)) 
	}

	private last3Entities(arr: IPublshedEntity[]) : IPublshedEntity[] {
		return _(arr).filter({published: true})
				.sortBy(el => (<any>el).date)
				.reverse()
				.take(3)
				.valueOf()
	}
	// INITIAL STATE
	public getInitState() {
		this.getCore('/api/initState', INITIAL_STATE_LOAD);
	}

	// SETTINGS
	public getSettings() {
		this.getCore('/api/settings', SETTINGS_LOAD);
	}
	public putSettings(settings: Settings) {
		return this.putCore(settings, '/api/settings', SETTINGS_PUT);
	}

	// CALCULATOR
	public getCalculator() {
		this.getCore('/api/calculator', CALCULATOR_LOAD);
	}
	public putCalculator(calculator: Calculator) {
		return this.putCore(calculator, '/api/calculator', CALCULATOR_PUT);
	}

	// REVIEWS
	public getReviews()  {
		this.getCore('/api/reviews', REVIEW_LOAD);
	}
	public postReview(review: Review) {
		this.postCore(review, '/api/reviews', REVIEW_POST);
	}
	public putReview(review: Review) {
		this.putCore(review, '/api/reviews', REVIEW_PUT);
	}
	public deleteReview(_id) {
		this.deleteCore(_id, '/api/reviews', REVIEW_DELETE);
	}
	
	// ClientRequest
	public postClientRequest(req: ClientRequest) {
		this.http.post('/api/clientRequest', req)
			.catch(this.handleError)
			.subscribe(res => {})
	}

	// RENOVA
	public postRenova(renova: Renova) {
		this.postCore(renova, '/api/renovas', RENOVA_POST);
	}
	public putRenova(renova: Renova) {
		this.putCore(renova, '/api/renovas', RENOVA_PUT);
	}
	public deleteRenova(_id: any) {
		this.deleteCore(_id, '/api/renovas', RENOVA_DELETE);
	}

	// PRICELISTS
	public postPriceList(priceList: PriceList) {
		this.postCore(priceList, '/api/priceLists', PRICELIST_POST);
	}
	public putPriceList(priceList: PriceList) {
		this.putCore(priceList, '/api/priceLists', PRICELIST_PUT);
	}
	public deletePriceList(_id: any) {
		this.deleteCore(_id, '/api/priceLists', PRICELIST_DELETE);
	}

	// ARTICLES
	public getArticles() {
		this.getCore('/api/articles', ARTICLE_GET);
	}
	public postArticle(article: Article) {
		this.postCore(article, '/api/articles', ARTICLE_POST);
	}
	public putArticle(article: Article) {
		this.putCore(article, '/api/articles', ARTICLE_PUT);
	}
	public deleteArticle(_id: any) {
		this.deleteCore(_id, '/api/articles', ARTICLE_DELETE);
	}

	// Portfolio Works
	public getPortfolioWorks() {
		this.getCore('/api/works', PORTFOLIOWORKS_GET);
	}
	public postPortfolioWork(work: Article) {
		this.postCore(work, '/api/works', PORTFOLIOWORKS_POST);
	}
	public putPortfolioWork(work: Article) {
		this.putCore(work, '/api/works', PORTFOLIOWORKS_PUT);
	}
	public deletePortfolioWork(_id: any) {
		this.deleteCore(_id, '/api/works', PORTFOLIOWORKS_DELETE);
	}

	// Videos
	public getVideos() {
		this.getCore('/api/videos', VIDEOS_GET);
	}
	public postVideo(video: Video) {
		this.postCore(video, '/api/videos', VIDEOS_POST);
	}
	public putVideo(video: Video) {
		this.putCore(video, '/api/videos', VIDEOS_PUT);
	}
	public deleteVideo(_id: any) {
		this.deleteCore(_id, '/api/videos', VIDEOS_DELETE);
	}


	// PRIVATE
	private getCore(api: string, actionType) {
		this.http.get(api)
				.catch(this.handleError)
				.subscribe((res: Response) => {
					console.log(actionType);
					this.store.dispatch({ 
						type: actionType, 
						payload: res.json().data || {}
					});
				})
	}	
	private postCore<T extends Document<T>>(entity: T, api: string, actionType) {
		this.http.post(api, entity)
			.catch(this.handleError)
			.subscribe((res: Response) => {
				entity._id = res.json().data._id;
				this.store.dispatch({ type: actionType, payload: entity });
			})
	}
	private putCore<T extends Document<T>>(entity: T, api: string, actionType) {
		this.http.put(api, entity)
			.catch(this.handleError)
			.subscribe((res: Response) => {
				this.store.dispatch({ type: actionType, payload: entity });
			});
	}
	private deleteCore(_id, api: string, actionType) {
		return this.http.delete(api+'/'+_id)
			.catch(this.handleError)
			.subscribe((res: Response) => this.store.dispatch({ type: actionType, payload: {_id: _id} }));
	}

	private handleError(error: any) {
		// In a real world app, we might use a remote logging infrastructure
		// We'd also dig deeper into the error to get a better message
		let errMsg = (error.message) ? error.message :
			error.status ? `${error.status} - ${error.statusText}` : 'Server error';
		console.error(errMsg); // log to console instead
		return Observable.throw(errMsg);
	}
}

interface IPublshedEntity {
	published: boolean;
	date: Date;
}