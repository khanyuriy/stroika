import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { SeoService } from '../seo.service';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { AppState, HOME_SET_HEADER_IMAGE } from '../../store';
import { Settings, Review, Article, PortfolioWork, Video } from '../../../domain';
import { AppService } from '../../app.service';
import { Router, NavigationEnd } from '@angular/router';
import { WINDOW_SERVICE, WindowService } from '../../../window.service';
import * as _ from 'lodash';

@Component({
	templateUrl: 'mainPage.component.html',
	styleUrls: ['mainPage.component.css']
})
export class MainPageComponent implements OnInit, OnDestroy {
	settings$: Observable<Settings>;
	reviews$: Observable<Review[]>;
	articles$: Observable<Article[]>;
	works$: Observable<PortfolioWork[]>;
	lastVideos$: Observable<Video[]>;
	settingsSub: Subscription;
	routerSub: Subscription;
	
	constructor(
		private store: Store<AppState>,
		private servce: AppService,
		private seoService: SeoService,
		private router: Router,
		@Inject(WINDOW_SERVICE) private windowService: WindowService
	) { }

	ngOnInit() {
		this.settings$ = this.store.select(state => state.settings);
		this.settingsSub = this.settings$.subscribe(s => {
			if (s) {
				this.seoService.setTitle(s.mainPageTitle);
			}
		});

		this.reviews$ = this.servce.lastReviews$;
		this.articles$ = this.servce.lastArticles$;
		this.works$ = this.servce.lastWorks$;
		this.lastVideos$ = this.servce.videos$.map(videos => _.take(videos, 6));

		this.routerSub = this.router.events
			.filter(evt => evt instanceof NavigationEnd)		
			.subscribe(evt => this.windowService.scrollToTop());

	 }

	 scrollToTop() {
		
	 }

	 click() {
		 alert(123);
	 }

	 ngOnDestroy() {
		 if (this.settingsSub) this.settingsSub.unsubscribe();
		 if (this.routerSub) this.routerSub.unsubscribe();
	 }
}