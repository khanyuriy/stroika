import { Component, OnInit } from '@angular/core';
import { Video } from '../../../domain';
import { Observable } from 'rxjs/Observable';
import { AppService } from '../../app.service';

@Component({
	templateUrl: 'videos.component.html'
})
export class VideosComponent implements OnInit {
	videos$ : Observable<Video[]>;
	constructor(
		private service: AppService
	) { }

	ngOnInit() { 
		this.videos$ = this.service.videos$;
	}

}