import { Directive, OnInit, ElementRef, AfterViewInit, Input } from '@angular/core';
import {config} from '../../../config';

declare var jQuery;

@Directive({
	selector: 'video-item'
})
export class VideoItemDirective implements OnInit, AfterViewInit {
	@Input() youtubeId: string;
	constructor(
		private elementRef: ElementRef
	) { }

	ngOnInit() { }

	ngAfterViewInit() {
		if (typeof jQuery != "undefined") {
			jQuery(this.elementRef.nativeElement).lazyYT(config.youtubeApiKey, {id: this.youtubeId});
		}
		
	}
}