import {AppService} from '../../app.service';
import {Component, OnInit, Input} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Video } from '../../../domain';

@Component({
	selector: 'last-video',
	template: `
		<div *ngFor="let video of videos" class="g-mb-20">
			<video-item class="lazyYT" [youtubeId]="video.url" data-ratio="16:9" data-display-duration="true">загрузка...</video-item>
		</div>
	`
})
export class LastVideoComponent {
	@Input() videos: Video[];
}