import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { SearchResult, Article } from '../../../domain';
import { AppState, SearchState, SEARCH_SET_RESULTS } from '../../store';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash'; 

@Injectable()
export class SearchService {

	sub: any;

	constructor(
		private store: Store<AppState>
	) { }

	public search(query: string, state: AppState) {
		let result = new SearchResult();
		let re = new RegExp(query, 'ig');
		result.articles = _.filter(state.articles, a => a.published && (a.title && a.title.match(re) || a.text && a.text.match(re)) 
		);
		result.priceLists = _.filter(state.priceLists, pl => pl.name && pl.name.match(re));
		result.renovas = _.filter(state.renovas, r => r.published && (r.menuText && r.menuText.match(re) || r.text && r.text.match(re)));
		result.reviews = _.filter(state.reviews, r => r.comment && r.comment.match(re) || r.author && r.author.match(re));
		result.videos = _.filter(state.videos, r => r.published && (r.title && r.title.match(re) || r.annotation && r.annotation.match(re)));
		result.works = _.filter(state.portfolioWorks, r => r.published && (r.title && r.title.match(re) || r.annotation && r.annotation.match(re) || r.text && r.text.match(re)));

		let theState: SearchState = {
			query: query,
			result: result
		}
		this.store.dispatch({ type: SEARCH_SET_RESULTS, payload: theState });
	}
}