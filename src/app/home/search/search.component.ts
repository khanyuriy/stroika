import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState, SearchState } from '../../store';
import { ActivatedRoute } from '@angular/router'; 
import { SearchService } from '../search';
import { SearchResult } from '../../../domain';
import { Observable } from 'rxjs/Observable';

@Component({
	templateUrl: 'search.component.html',
	styleUrls: ['search.component.css']
})
export class SearchComponent implements OnInit {
	sub: any;
	state$: Observable<SearchState>
	constructor(
		private route: ActivatedRoute,
		private searchService: SearchService,
		private store: Store<AppState>
	) { 
	}

	ngOnInit() { 
		this.state$ = this.store.select(s => s.search);

		this.sub = this.route.params.subscribe(params => {
			Observable.combineLatest(
				this.store.select(s => s.articles),
				this.store.select(s => s.portfolioWorks),
				this.store.select(s => s.priceLists),
				this.store.select(s => s.renovas),
				this.store.select(s => s.reviews),
				this.store.select(s => s.videos)
				
			).subscribe(([articles, portfolioWorks, priceLists, renovas, reviews, videos]) => {
				let state: AppState = {
					articles: articles,
					homeState: null,
					portfolioWorks: portfolioWorks,
					priceLists: priceLists,
					renovas: renovas,
					reviews: reviews,
					videos: videos,
					settings: null,
					search: null,
					calculator: null
				}
				
				this.searchService.search(params['query'], state);
			})
		});
	}

	ngOnDestroy() {
		this.sub.unsubscribe();
	}
}