import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ClientRequest } from '../../../domain';
import { AppService } from '../../app.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, NavigationEnd } from '@angular/router'; 
import { WINDOW_SERVICE, WindowService } from '../../../window.service';

@Component({
	templateUrl: 'request.component.html',
	styleUrls: ['request.component.css']
})
export class RequestComponent implements OnInit, OnDestroy {
	requestForm: FormGroup;
	submited: boolean;
	routerSub: Subscription;

	constructor(
		private service: AppService, 
		private builder: FormBuilder,
		private router: Router,
		@Inject(WINDOW_SERVICE) private windowService: WindowService
	) {
		this.requestForm = builder.group({
			fio: [''],
			email: [''],
			phone: [''],
			comment: ['']
		}, {
			validator : this.emailOrPhoneRequiredValidator
		});
		this.submited = false;

		this.routerSub = this.router.events.filter(evt => evt instanceof NavigationEnd)
							.subscribe(evt => windowService.scrollToTop() );
	}

	ngOnInit() { }

	send() {
		if (this.requestForm.valid) {
			var req = new ClientRequest();
			req.fio = this.requestForm.value.fio;
			req.email = this.requestForm.value.email;
			req.phone = this.requestForm.value.phone;
			req.comment = this.requestForm.value.comment;
			
			this.service.postClientRequest(req);
			this.submited = true;
		}
	}

	emailOrPhoneRequiredValidator(group: FormGroup) {
		let emailInput = group.controls['email'];
		let phoneInput = group.controls['phone'];
		if (!emailInput.value && !phoneInput.value) {
			return { emailAndPhoneAreEmpty: true }
		} else {
			return null;
		}
	}

	ngOnDestroy() {
		if (this.routerSub) this.routerSub.unsubscribe();
	}
}