import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Review } from '../../../domain';

@Component({
	selector: 'review',
	templateUrl: 'review.component.html',
	styleUrls: ['review.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReviewComponent implements OnInit {
	@Input()
	review: Review;

	constructor() { }

	ngOnInit() { }

}