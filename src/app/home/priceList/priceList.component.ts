import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { PriceList, Units } from '../../../domain';
import * as _ from 'lodash';
import { Router, NavigationEnd } from '@angular/router'; 
import { WINDOW_SERVICE, WindowService } from '../../../window.service';

@Component({
	templateUrl: 'priceList.component.html',
	styleUrls: ['priceList.component.css']
})
export class PriceListComponent implements OnInit, OnDestroy {
	priceLists$: Observable<PriceList[]>;
	routerSub: Subscription;
	constructor(
		private store: Store<AppState>,
		private router: Router,
		@Inject(WINDOW_SERVICE) private windowService: WindowService
	) { 
		this.routerSub = this.router.events.filter(evt => evt instanceof NavigationEnd)
							.subscribe(evt => windowService.scrollToTop());
	}

	ngOnInit() { 
		this.priceLists$ = this.store.select(s => s.priceLists).map(priceLists => _.filter(priceLists, { published: true }));
	}

	ngOnDestroy() {
		if (this.routerSub) this.routerSub.unsubscribe();
	}
}