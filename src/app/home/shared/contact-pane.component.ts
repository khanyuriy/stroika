import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { Observable } from 'rxjs/Observable';
import { Settings } from '../../../domain';

@Component({
	selector: 'contact-pane',
	template: `
		<div>Более детальную информацию Вы можете узнать позвонив нам по телефону: <span class="phone2">{{(settings$ | async)?.phone}}</span></div><br />
		<div>Либо мы перезвоним Вам - <a class="request" routerLink="/request">Заказать звонок</a></div>
	`,
	styles: [
		`
			.phone2 {
				color: #363436;
				font-family: 'PT Sans Caption',sans-serif;
				font-size: 18px;
				font-weight: bold;
				text-align: center;
				white-space: nowrap;
			}

			.request {
				margin: 0px;
				padding: 0px;
				border-width: 0px 0px 1px;
				border-top-style: initial;
				border-right-style: initial;
				border-bottom-style: dashed;
				border-left-style: initial;
				border-top-color: initial;
				border-right-color: initial;
				border-bottom-color: #f12b24;
				border-left-color: initial;
				font-style: inherit;
				font-variant: inherit;
				font-weight: bold;
				font-stretch: inherit;
				font-size: 14px;
				line-height: inherit;
				font-family: inherit;
				vertical-align: baseline;
				color: #f12b24;
			}
		`
	]
})
export class ContactPaneComponent implements OnInit {
	settings$: Observable<Settings>;
	constructor( private store: Store<AppState> ) { 
		this.settings$ = store.select(s => s.settings);
	}
	ngOnInit() { }
}