import { Component, Input } from '@angular/core';

@Component({
	selector: 'section-header',
	templateUrl: 'section-header.component.html',
	styleUrls: ['section-header.component.css']
})
export class SectionHeaderComponent {
	@Input() header: string;
	@Input() title: string; 
	@Input() annotation: string;
	@Input() style: string;
}