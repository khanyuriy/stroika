import {Settings} from '../../../domain';
import {Component, OnInit, OnDestroy, Inject} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { AppState } from '../../store';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { WINDOW_SERVICE, WindowService } from '../../../window.service';

@Component({
	templateUrl: 'contacts.component.html',
	styleUrls: ['contacts.component.css']
})
export class ContactsComponent implements OnInit, OnDestroy {
	settings$: Observable<Settings>;
	routerSub: Subscription;

	constructor(
		private store: Store<AppState>,
		private router: Router,
		@Inject(WINDOW_SERVICE) private windowService: WindowService
	) { 
		this.routerSub = this.router.events.filter(evt => evt instanceof NavigationEnd)
							.subscribe(evt => windowService.scrollToTop() );
	}

	ngOnInit() { 
		this.settings$ = this.store.select(s => s.settings);
	}

	 ngOnDestroy() {
		 if (this.routerSub) this.routerSub.unsubscribe();
	 }
}