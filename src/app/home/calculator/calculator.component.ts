import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { WINDOW_SERVICE, WindowService } from '../../../window.service';

@Component({
	templateUrl: 'calculator.component.html',
})
export class CalculatorComponent implements OnInit, OnDestroy {
	routerSub: Subscription;

	constructor(
		private router: Router,
		@Inject(WINDOW_SERVICE) private windowService: WindowService
	) {
		this.routerSub = this.router.events
			.filter(evt => evt instanceof NavigationEnd)
			.subscribe(evt => windowService.scrollToTop());
	}

	ngOnInit() { }

	ngOnDestroy() {
		if (this.routerSub) this.routerSub.unsubscribe();
	}
}