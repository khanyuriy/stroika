import { Component, OnInit, Input } from '@angular/core';
import { Article } from '../../../domain';
import { Router } from '@angular/router';
import * as _ from 'lodash';

@Component({
	selector: 'last-articles',
	styleUrls: ['lastArticles.component.css'],
	templateUrl: 'lastArticles.component.html'
})
export class LastArticlesComponent implements OnInit {
	@Input() articles: Article[];
	constructor(
	) { }

	ngOnInit() { 
	}
}