import { Component, OnInit } from '@angular/core';
import { Article } from '../../../domain';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Component({
	templateUrl: 'article-feed.component.html',
	styleUrls: ['article-feed.component.css']
})
export class ArticleFeedComponent implements OnInit {
	articles$: Observable<Article[]>;
	constructor(
		private store: Store<AppState>
	) { }

	ngOnInit() { 
		this.articles$ = this.store.select(s=> s.articles);
	}

	getArticlePreview(article: Article) {
		return article.text.substr(0, 3000);
	}

}