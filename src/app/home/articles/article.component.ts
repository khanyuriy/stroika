import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { AppService } from '../../app.service';
import { Article } from '../../../domain';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState, HOME_SET_HEADER_IMAGE } from '../../store';
import * as _ from 'lodash';
import { Subscription } from 'rxjs/Subscription';
import { WINDOW_SERVICE, WindowService } from '../../../window.service';



@Component({
	templateUrl: 'article.component.html',
	styleUrls: ['article.component.css']
})
export class ArticleComponent implements OnInit, OnDestroy {
	article: Article;
	code: string;
	routerSub: Subscription;
	
	constructor(
		private store: Store<AppState>,
		private route: ActivatedRoute,
		private router: Router,
		@Inject(WINDOW_SERVICE) private windowService: WindowService  
	) { 
		this.code = route.snapshot.params['code'];
		this.routerSub = this.router.events
			.filter(evt => evt instanceof NavigationEnd)
			.subscribe(evt => windowService.scrollToTop() );
	}

	ngOnInit() { 
		this.store.select(s => s.articles).subscribe(articles => {
			this.article = _.find(articles, {code: this.code});
			this.store.dispatch({ type: HOME_SET_HEADER_IMAGE, payload: { headerImageUrl: this.article.imageUrl }});
		});
	}

	 ngOnDestroy() {
		 if (this.routerSub) this.routerSub.unsubscribe();
	 }

}