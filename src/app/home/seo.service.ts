import { Injectable, Inject, ElementRef, Renderer } from '@angular/core'
import { DOCUMENT } from '@angular/platform-browser'

@Injectable()
export class SeoService {    
	//--------------------------------------------
	//! Set the variables to their default values
	//--------------------------------------------
	constructor (@Inject(DOCUMENT) private document) {
	}

	//----------------------------
	//! Set the title of the page
	//----------------------------
	setTitle (title: string) {
		this.document.title = title
	}
}