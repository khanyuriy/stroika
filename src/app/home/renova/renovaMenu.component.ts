import { Component, OnInit, Input } from '@angular/core';
import { Renova } from '../../../domain';


@Component({
	selector: 'renova-menu',
	template: `
			<div id="renova-list">
				<div id="flatheader">НАШИ УСЛУГИ</div>
				<ul class="menu1">

					<template ngFor let-renova [ngForOf]="renovas">
						<li>
							<a [routerLink]="['/renova', renova.code]">{{renova.menuText}}</a>
						</li>
						<hr>
					</template>
				</ul>
			</div>
	`
})
export class RenovaMenuComponent implements OnInit {
	@Input() renovas: Renova[];

	constructor() { }

	ngOnInit() { }
}