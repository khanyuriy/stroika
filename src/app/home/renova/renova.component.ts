import { Component, OnInit, OnDestroy, ViewEncapsulation, Inject } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router'; 
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppState, HOME_SET_HEADER_IMAGE } from '../../store';
import { Renova, PortfolioWork, Video } from '../../../domain';
import { AppService } from '../../app.service';
import * as _ from 'lodash';
import { WINDOW_SERVICE, WindowService } from '../../../window.service';


@Component({
	templateUrl: 'renova.component.html',
	styleUrls: ['renova.component.css']
})
export class RenovaComponent implements OnInit, OnDestroy {
	sub: Subscription;
	renovaCode: string;
	renovas$: Observable<Renova[]>;
	lastWorks$: Observable<PortfolioWork[]>;
	lastVideos$: Observable<Video[]>;
	text: string;
	menuText: string;
	routerSub: Subscription;
	constructor(
		private route: ActivatedRoute,
		private store: Store<AppState>,
		private router: Router,
		private service: AppService,
		@Inject(WINDOW_SERVICE) private windowService: WindowService
	) {	}

	ngOnInit() {
		this.renovas$ = this.store.select(s => s.renovas);
		this.sub = Observable.combineLatest([
			this.route.params,
			this.store.select(s => s.renovas)
		]).subscribe(result => {
			let params: {[key: string] : any} = result[0];
			let renovas: Renova[] = result[1];
			if (params && renovas) {
				this.renovaCode = params['code'];
				let renova = _.find(renovas, {code: this.renovaCode});
				this.text = renova && renova.text;
				this.menuText = renova && renova.menuText;
				this.store.dispatch({ type: HOME_SET_HEADER_IMAGE, payload: {headerImageUrl: renova.headerImageUrl }})
				this.lastWorks$ = this.service.lastWorksByRenova(renova._id); 	
			}
		});
		this.lastVideos$ = this.service.lastVideos$;
		this.routerSub = this.router.events.filter(evt => evt instanceof NavigationEnd)
							.subscribe(evt => this.windowService.scrollToTop() );
	}

	ngOnDestroy() {
		this.sub && this.sub.unsubscribe();
		if (this.routerSub) this.routerSub.unsubscribe();
	}
}