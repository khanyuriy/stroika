import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { Renova } from '../../../domain';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

@Component({
	selector: 'renova-list',
	templateUrl: 'renovaList.component.html',
	styleUrls: ['renovaList.component.css']
})
export class RenovaListComponent implements OnInit {
	renovas$: Observable<Renova[]>;

	constructor(
		private store: Store<AppState>
	) { }

	ngOnInit() {
		this.renovas$ = this.store.select(s => s.renovas).map(renovas => _.sortBy(renovas, r => r.menuText));
	}
}