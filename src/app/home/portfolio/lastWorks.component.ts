import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { PortfolioWork, Renova } from '../../../domain';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import * as _ from 'lodash';

@Component({
	selector: 'last-works',
	templateUrl: 'lastWorks.component.html',
	styleUrls: ['lastWorks.component.css']
})
export class LastWorksComponent implements OnInit, OnDestroy {
	@Input() works: PortfolioWork[];
	renovas: Renova[];
	sub: any;

	constructor(
		private store: Store<AppState>
	) { }

	ngOnInit() { 
		this.sub = this.store.select(state => state.renovas).subscribe(renovas => {
			this.renovas = renovas;
		})
	}

	getWorkRenovaName(work: PortfolioWork) {
		let renova = this.renovas && _.find(this.renovas, {_id: work.renovaId});
		return renova && renova.menuText;
	}

	ngOnDestroy() {
		this.sub.unsubscribe();
	}
}