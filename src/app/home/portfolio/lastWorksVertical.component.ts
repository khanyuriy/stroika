import { Component, OnInit } from '@angular/core';
import { LastWorksComponent } from './lastWorks.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';

@Component({
	selector: 'last-works-vertical',
	templateUrl: 'lastWorksVertical.component.html'
})
export class LastWorkVerticalComponent extends LastWorksComponent {
	constructor(
		store: Store<AppState>
	) {
		super(store);
	}
}