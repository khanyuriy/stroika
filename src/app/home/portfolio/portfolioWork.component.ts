import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppService } from '../../app.service';
import { PortfolioWork } from '../../../domain';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import * as _ from 'lodash';

@Component({
	templateUrl: 'portfolioWork.component.html',
	styleUrls: ['portfolioWork.component.css']
})
export class PortfolioWorkComponent implements OnInit, OnDestroy {
	work: PortfolioWork;
	code: string;
	routerSub: any;
	constructor(
		private store: Store<AppState>,
		private route: ActivatedRoute,
		private router: Router
	) { 
		this.code = route.snapshot.params['code'];
		this.routerSub = router.events.subscribe(s => {
			if (s instanceof NavigationEnd) {
				const tree = router.parseUrl(router.url);
				if (tree.fragment) {
					const element: any = document.querySelector("#" + tree.fragment);
					if (element) { element.scrollIntoView(element); }
				}
			}
		});
	}

	ngOnInit() {
		this.store.select(s => s.portfolioWorks).subscribe(works => {
			this.work = _.find(works, {code: this.code});
		});
	}
	ngOnDestroy() {
		this.routerSub.unsubscribe();
	}
}