import { Component, OnInit, Input } from '@angular/core';
import { PortfolioWork } from '../../../domain';
import * as _ from 'lodash';

@Component({
	selector: 'work-card',
	templateUrl: 'workCard.component.html',
	styleUrls: ['workCard.component.css']
})
export class WorkCardComponent implements OnInit {
	@Input() work: PortfolioWork;
	constructor() { }

	ngOnInit() { }

	getPhotos() {
		return _.take(this.work.photos, 4);
	}

}