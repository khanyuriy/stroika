import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { PortfolioWork } from '../../../domain';
import { Observable } from 'rxjs/Observable';

@Component({
	templateUrl: 'portfolio.component.html',
	styleUrls: ['portfolio.component.css']
})
export class PortfolioComponent implements OnInit {
	works$: Observable<PortfolioWork[]>;
	constructor(
		private store: Store<AppState>
	) { 
		
	}

	ngOnInit() {
		this.works$ = this.store.select(state => state.portfolioWorks);
	}

}