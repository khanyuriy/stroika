export * from './lastWorks.component';
export * from './portfolio.component';
export * from './portfolioWork.component';
export * from './workCard.component';
export * from './lastWorksVertical.component';