import {OpaqueToken, ElementRef} from '@angular/core';
import {YmapsNodeService} from './ymaps.service.node';
import {YmapsBrowserService} from './ymaps.service.browser';
import { Settings } from '../../../domain';


export const YMAPS_SERVICE : OpaqueToken = new OpaqueToken('YmapsService');

export interface YmapsService {
	init(element: ElementRef, settings: Settings);
}

export var provideYmapsBrowser = function () {
	return { provide: YMAPS_SERVICE, useValue: new YmapsBrowserService() }
}

export var provideYmapsNode = function () {
	return { provide: YMAPS_SERVICE, useValue: new YmapsNodeService() }
}