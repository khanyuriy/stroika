import { Directive, ElementRef, Input, Renderer, OnInit, AfterViewInit, Inject } from '@angular/core';
import { YmapsService, YMAPS_SERVICE } from './ymaps.common';
import { Settings } from '../../../domain';


@Directive({
	selector: '[ymaps]'
})
export class YmapsDirective implements OnInit, AfterViewInit {
	@Input() settings: Settings;
	constructor(private element: ElementRef, private renderer: Renderer,
		@Inject(YMAPS_SERVICE) private service: YmapsService) {
		console.log('YmapsDirective ctor');
	}

	ngOnInit() {
	}

	ngAfterViewInit() {
		setTimeout(() => {
			this.service.init(this.element, this.settings);
		});
	}
}