import { YmapsService } from './ymaps.common';
import { ElementRef } from '@angular/core';
import { Settings } from '../../../domain';

declare var ymaps: any;

export class YmapsBrowserService implements YmapsService {
	init(element: ElementRef, settings: Settings) {
		console.log('YmapsBrowserService');

		ymaps.ready(_ => {
			let point = [settings.mapY, settings.mapX];
			var myMap = new ymaps.Map(element.nativeElement, {
					center: point,
					zoom: 14,
					controls: ['zoomControl']
				}),
				myGeoObject = new ymaps.GeoObject({
					geometry: {
						type: "Point",// тип геометрии - точка
						coordinates: point // координаты точки
				}
				});
			myMap.geoObjects.add(myGeoObject);
			myMap.behaviors.disable('scrollZoom');
		})
	}
}