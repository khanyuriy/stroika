import {Component, ComponentFactory, NgModule, Input, Injectable} from '@angular/core';
import { RuntimeCompiler,  } from '@angular/compiler';
import { SharedModule } from '../../shared/shared.module';
import * as _ from 'lodash';

@Injectable()
export class DynamicTypeBuilder {

	// wee need Dynamic component builder
	constructor(protected compiler: RuntimeCompiler) { }

	// this object is singleton - so we can use this as a cache
private _cacheOfFactories: {[templateKey: string]: ComponentFactory<any>} = {};

public createComponentFactory(template: string)
	: Promise<ComponentFactory<any>> {

	let factory = this._cacheOfFactories[template];

	if (factory) {
		console.log("Module and Type are returned from cache")
	
		return new Promise((resolve) => {
			resolve(factory);
		});
	}
	
	// unknown template ... let's create a Type for it
	let type   = this.createNewComponent(template);
	let module = this.createComponentModule(type);
	
	return new Promise((resolve) => {
		this.compiler
			.compileModuleAndAllComponentsAsync(module)
			.then((moduleWithFactories) =>
			{
				factory = _.find(moduleWithFactories.componentFactories, { componentType: type });
				this._cacheOfFactories[template] = factory;
				resolve(factory);
			});
	});
}

protected createNewComponent (tmpl:string) {
	@Component({
		template: tmpl
	})
	class CustomDynamicComponent  { };
	// a component for this particular template
	return CustomDynamicComponent;
}
protected createComponentModule (componentType: any) {
	@NgModule({
		imports: [
			SharedModule, // there are 'text-editor', 'string-editor'...
		],
		declarations: [
			componentType
		],
	})
	class RuntimeComponentModule
	{
	}
	// a module for just this Type
	return RuntimeComponentModule;
}
}