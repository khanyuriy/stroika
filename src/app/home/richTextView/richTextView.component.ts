import { 
	Component, ComponentRef, ViewChild, ViewContainerRef,
	Input,
	AfterViewInit, OnInit, OnDestroy,
	OnChanges, SimpleChange, ComponentFactory,
	ViewEncapsulation
 } from '@angular/core';
import { DynamicTypeBuilder } from './dynamicTypeBuilder.service'; 
import { RuntimeCompiler } from '@angular/compiler';

@Component({
	moduleId: __filename,
	selector: 'rich-text-view',
	template: `
		<div class="rich-text-view">	
			<div #dynamicContentPlaceholder></div>
		</div>
	`
})
export class RichTextViewComponent implements OnInit, AfterViewInit, OnDestroy {
	@Input() text: string;
	@ViewChild('dynamicContentPlaceholder', {read: ViewContainerRef}) 
	dynamicComponentTarget: ViewContainerRef;

	protected componentRef: ComponentRef<any>;

	protected wasViewInitialized = false;

	constructor(
		private typeBuilder: DynamicTypeBuilder
	) { }

	ngOnInit() {
	}

	// this is the best moment where to start to process dynamic stuff
	public ngAfterViewInit(): void
	{
		this.wasViewInitialized = true; 
		this.refreshContent();
	}

	// wasViewInitialized is an IMPORTANT switch 
	// when this component would have its own changing @Input()
	// - then we have to wait till view is intialized - first OnChange is too soon
	public ngOnChanges(changes: {[key: string]: SimpleChange}): void
	{
		if (!this.wasViewInitialized) {
			return;
		}
		this.refreshContent();
	}

	public ngOnDestroy(){
		if (this.componentRef) {
			this.componentRef.destroy();
			this.componentRef = null;
		}
	}

	protected refreshContent(useTextarea: boolean = false) {
	
		if (this.componentRef) {
			this.componentRef.destroy();
		}
		
		// here we get a TEMPLATE with dynamic content === TODO
		if (!this.text) return;
		var template = this.prepareTemplate(this.text);

		// here we get Factory (just compiled or from cache)
		this.typeBuilder
			.createComponentFactory(template)
			.then(factory =>
			{
				// Target will instantiate and inject component (we'll keep reference to it)
				this.componentRef = this
					.dynamicComponentTarget
					.createComponent(factory);
			});
	}

	private prepareTemplate(text: string) : string {
		text = this.findAndReplacePriceList(text);
		return text
			.replace(/[<(&lt;)]+заявка[>(&gt;)]+/ig, '<request></request>')
			.replace(/[<(&lt;)]+звонок[>(&gt;)]+/ig, '<callback></callback>')
			.replace(/[<(&lt;)]+рассчитать[>(&gt;)]+/ig, '<to-calc></to-calc>');
	}

	private findAndReplacePriceList(text: string) : string {
		let token = /[<(&lt;)]+прайслист\s*".*"[>(&gt;)]+/ig.exec(text);
		if (!token || token.length == 0) {
			return text;
		}

		let param = token && token.length > 0 && /".*"/ig.exec(token[0]) || null;

		if (!param) {
			return text.replace(token[0], '');
		}

		let plCode = param && param.length > 0 && param[0];
		return text.replace(token[0], `<price-list-view priceListCode="${plCode.replace(/"/ig, '')}"></price-list-view>`);
	}
}