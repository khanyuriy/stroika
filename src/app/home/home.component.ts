import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { AppState, HomeState, HOME_SET_HEADER_IMAGE } from '../store';
import { Settings, Renova, PortfolioWork, Article } from '../../domain';
import { Router } from '@angular/router';
import { LastWorksComponent } from './portfolio';
import { SearchService } from './search';
import { AppService } from '../app.service';
import * as _ from 'lodash';


class SplitedRenovas {
	first: Renova[];
	second: Renova[];
}

const MAX_ROWS_IN_COLUMN = 6;


@Component({
	moduleId: __filename,
	styleUrls: [
		'home.style.css'
	],
	templateUrl: 'home.template.html',
	providers: [SearchService]
})
export class HomeComponent implements OnInit, OnDestroy {
	settings$: Observable<Settings>;
	renovas$: Observable<Renova[]>;
	lastArticles$: Observable<Article[]>;
	homeState$: Observable<HomeState>;
	
	currentRenova: Renova;
	works$: Observable<PortfolioWork[]>;
	settingsSub: Subscription;
	searchQuery: string;
	settings: Settings;
	constructor(
		private store: Store<AppState>, 
		private router: Router,
		private service: AppService
		) { }

	ngOnInit() {
		this.settings$ = this.store.select(state => state.settings);
		
		this.homeState$ = this.store.select(state => state.homeState);
		
		this.works$ = this.service.lastWorks$;

		this.renovas$ = this.service.renovas$.map(renovas => _.sortBy(renovas, r => r.menuText));
 
		this.lastArticles$ = this.service.lastArticles$;

		this.settingsSub = this.settings$.subscribe(s => {
			this.settings = s;
		})
	}

	search() {
		this.router.navigate(['/search', this.searchQuery]);
	}

	isRenovaSelected(renova: Renova) {
		return this.currentRenova && this.currentRenova._id == renova._id;
	}

	isMainPage() {
		return this.currentRenova == null;
	}

	getHeaderStyle() : any{
		if (!this.settings) return null;

		return {
			'background-image': 'url('+this.settings.headerImageUrl+')',
			'background-repeat': 'no-repeat',
			'background-position': 'center',
			'background-size': 'cover'
		};
	 }

	ngOnDestroy() {
		this.settingsSub.unsubscribe();
	}
}
