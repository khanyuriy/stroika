import { 
	HomeComponent,
	MainPageComponent,
	SocialsComponent,
	YmapsDirective,
	AdvantagesComponent,
	NotFoundComponent,
	ReviewComponent,
	LastArticlesComponent,
	LastWorksComponent, PortfolioComponent, PortfolioWorkComponent, WorkCardComponent,
	RenovaListComponent, RenovaComponent,
	ContactsComponent,
	PriceListComponent,
	ArticleFeedComponent, ArticleComponent,
	VideosComponent, VideoItemDirective,
	SearchComponent,
	SectionHeaderComponent,
	RichTextViewComponent,
	DynamicTypeBuilder,
	LastWorkVerticalComponent,
	LastVideoComponent,
	SeoService,
	RequestComponent,
	CalculatorComponent,
	ContactPaneComponent,
	RenovaMenuComponent
} from './home';

import { AppService } from './app.service';

export const APP_DIRECTIVES = [
	HomeComponent,
	MainPageComponent,
	SocialsComponent,
	YmapsDirective,
	AdvantagesComponent,
	ReviewComponent,
	LastArticlesComponent,
	LastWorksComponent, PortfolioComponent, PortfolioWorkComponent,
	RenovaListComponent, RenovaComponent,
	WorkCardComponent,
	ContactsComponent,
	PriceListComponent,
	ArticleFeedComponent, ArticleComponent,
	VideosComponent, VideoItemDirective,
	SearchComponent,
	NotFoundComponent,
	SectionHeaderComponent,
	RichTextViewComponent,
	LastWorkVerticalComponent,
	LastVideoComponent,
	RequestComponent,
	CalculatorComponent,
	ContactPaneComponent,
	RenovaMenuComponent
	
]

export const APP_SERVICES = [
	AppService,
	DynamicTypeBuilder,
	SeoService
]