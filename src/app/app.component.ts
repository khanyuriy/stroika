import { Component, Directive } from '@angular/core';
import { Http, Response } from '@angular/http';
import { AppService } from './app.service';
import { Store, Action } from '@ngrx/store';
import { AppState } from './store';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/let';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';

@Component({
	selector: 'app',
	styleUrls: ['app.component.css'],
	templateUrl: 'app.component.html'
})
export class AppComponent {
	constructor(private service: AppService, private http: Http, private store: Store<AppState>) { }

	ngOnInit() {
		this.service.getInitState();
	}

}
