import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';

import {
	HomeComponent,
	RenovaComponent,
	RenovaListComponent,
	MainPageComponent,
	ContactsComponent,
	PriceListComponent,
	ArticleFeedComponent,
	ArticleComponent,
	PortfolioComponent as HomePortfolioComponent,
	PortfolioWorkComponent,
	VideosComponent as HomeVideosComponent,
	SearchComponent,
	NotFoundComponent,
	RequestComponent,
	CalculatorComponent
} from './home';

export const routing: ModuleWithProviders = RouterModule.forRoot([
	{
		path: '', component: HomeComponent,
		children: [
			{path: '', component: MainPageComponent},
			{path: 'renovas', component: RenovaListComponent},
			{path: 'renova/:code', component: RenovaComponent},
			{path: 'contacts', component: ContactsComponent},
			{path: 'price', component: PriceListComponent},
			{path: 'articles', component: ArticleFeedComponent},
			{path: 'articles/:code', component: ArticleComponent},
			{path: 'portfolio', component: HomePortfolioComponent},
			{path: 'portfolio/:code', component: PortfolioWorkComponent},
			{path: 'vg', component: HomeVideosComponent},
			{path: 'search', redirectTo: 'search/'},
			{path: 'search/:query', component: SearchComponent},
			{path: 'request', component: RequestComponent},
			{path: 'calculator', component: CalculatorComponent}
		] 
	},
	{
		path: '**', component: NotFoundComponent
	}
]);