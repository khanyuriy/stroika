import { NgModule, ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 
import { CalculatorComponent } from './calculator/calculator.component';
import { CallbackComponent } from './callback/callback.component';

import { 
	DefaultButtonComponent, 
	PrimaryButtonComponent,
	DangerButtonComponent,
	SpecialButtonComponent
} from './ui/button.component';
import { 
	HFormGroupComponent,
	HFormLabelComponent,
	HFormInputComponent, 
	HButtonsPanelCopmonent,
	HorizontalFormComponent,
	CheckboxComponent
} from './ui/hform.component';

import { PreviewImageComponent } from './ui/image.component'
import { RequestComponent } from './request/request.component';
import { PriceListViewComponent, GroupsWithPrices } from './priceListView/priceListView.component';
import { Call2ActionComponent } from './call2action/call2action.component';
import { ToCalcComponent } from './toCalc/toCalc.component';
import { ApiService } from './api.service';
import { ModelService } from './model.service';

const MODULES = [
	CommonModule,
	FormsModule,
	ReactiveFormsModule,
	RouterModule
];

const COMPONENTS = [
	CalculatorComponent,

	DefaultButtonComponent,
	PrimaryButtonComponent,
	DangerButtonComponent,
	SpecialButtonComponent,

	HButtonsPanelCopmonent,
	HFormGroupComponent,
	HFormLabelComponent,
	HFormInputComponent,
	HorizontalFormComponent,
	PreviewImageComponent,
	CheckboxComponent,
	RequestComponent,
	Call2ActionComponent,
	PriceListViewComponent,
	SpecialButtonComponent,
	ToCalcComponent,
	CallbackComponent,
	GroupsWithPrices
]

const PROVIDERS: any[] = [
	ApiService, 
	ModelService, 
]

@NgModule({
	imports: [
		...MODULES
	],
	declarations: [
		...COMPONENTS
	],
	exports: [
		...MODULES,
		...COMPONENTS
	]
})
export class SharedModule {
	static forRoot() : ModuleWithProviders {
		return {
			ngModule: SharedModule,
			providers: [ ...PROVIDERS ]
		};
	}

}