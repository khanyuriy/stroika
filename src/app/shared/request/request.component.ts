import { Component, OnInit, Input } from '@angular/core';
import { ClientRequest } from '../../../domain';
import { AppService } from '../../app.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
	selector: 'request',
	templateUrl: 'request.component.html',
	styleUrls: ['request.component.css']
})
export class RequestComponent implements OnInit {
	requestForm: FormGroup;
	submited: boolean;
	@Input() actionTitle: string;

	constructor(private service: AppService, private builder: FormBuilder) {
		this.requestForm = builder.group({
			phone: ['', Validators.required]
		});
		this.submited = false;
	}

	ngOnInit() { }

	send() {
		var req = new ClientRequest();
		req.phone = this.requestForm.value.phone;
		this.service.postClientRequest(req);
		this.submited = true;
	}
}