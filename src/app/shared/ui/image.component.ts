import { Component, Input } from '@angular/core';

@Component({
	selector: 'preview-img',
	template: '<img class="preview-img" [src]="src" />'
})
export class PreviewImageComponent {
	@Input() src: string;
}