import { Component, Input } from '@angular/core';

export class ButtonComponent {
	@Input() size: string;
	@Input() disabled: boolean;
}

@Component({
	selector: 'default-button',
	template: `
		<button type="button" 
			[class.btn-xs]="size=='xs'" [disabled]="disabled"
			class="btn btn-default"><ng-content></ng-content></button>
	`
})
export class DefaultButtonComponent extends ButtonComponent {

}

@Component({
	selector: 'primary-button',
	template: `
		<button type="button" [disabled]="disabled"
			[class.btn-xs]="size=='xs'"
			class="btn btn-primary"><ng-content></ng-content></button>
	`
})
export class PrimaryButtonComponent extends ButtonComponent {

}

@Component({
	selector: 'danger-button',
	template: `
		<button type="button" [disabled]="disabled"
			[class.btn-xs]="size=='xs'"
			class="btn btn-danger"><ng-content></ng-content></button>
	`
})
export class DangerButtonComponent extends ButtonComponent {

}

@Component({
	selector: 'special-button',
	template: `
		<button class="btn-u btn-u--construction trim" [disabled]="disabled">
			<i class="g-mr-10 fa btn-u__fa fa-arrow-right"></i> 
			<ng-content></ng-content>
		</button>
	`,
	styles:[`
		.btn-u__fa {
			color: #f7b70b;
		}

		.btn-u--construction,
		.btn-u--construction:focus {
			color: #fff;
			background: #111;
			text-transform: uppercase;
			font-weight: bold;
			font-size: 11px;
			padding: 15px 30px;
		}
		.btn-u--construction:hover {
			background: #111;
			color: #fff;
			opacity: 0.8;
		}
		.btn-u--construction.trim::before {
		border: 2px solid #f7b70b;
		top: 8px;
		left: 8px;
		right: 8px;
		bottom: 8px;
		}
	`]
})
export class SpecialButtonComponent extends ButtonComponent {

}