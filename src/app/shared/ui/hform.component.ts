import { Component } from '@angular/core';

@Component({
	selector: 'hform',
	template: `
		<form class="form-horizontal">
			<ng-content></ng-content>
		</form>
	`
})
export class HorizontalFormComponent{
	
}

@Component({
	selector: 'hf-buttons',
	template: `
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<ng-content></ng-content>
			</div>
		</div>
	`
})
export class HButtonsPanelCopmonent {

}

@Component({
	selector: 'hf-group',
	template: `
		<div class="form-group">
			<ng-content></ng-content>
		</div>
	`
})
export class HFormGroupComponent {
	
}

@Component({
	selector: 'hf-label',
	template: `
		<label for="inputCode" class="col-sm-2 control-label">
			<ng-content></ng-content>
		</label>
	`
})
export class HFormLabelComponent {
	
}

@Component({
	selector: 'hf-input',
	template: `
		<div class="col-sm-10">
			<ng-content></ng-content>
		</div>
	`
})
export class HFormInputComponent {
	
}

@Component({
	selector: 'checkbox',
	template: `
		<div class="checkbox">
			<label>
				<ng-content></ng-content>
			</label>
		</div>
	`
})
export class CheckboxComponent {
	
}