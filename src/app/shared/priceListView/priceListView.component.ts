import { Component, OnInit, Input, Pipe, PipeTransform, Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { PriceList, PriceGroup, Units } from '../../../domain';
import { Observable } from 'rxjs/Observable';

import * as _ from 'lodash';

@Component({
	selector: 'price-list-view',
	template: `
		<table class="table table-hover table-responsive table-condensed price-list-view">
			<thead>
				<th style="text-align: left">Работа</th>
				<th style="width: 50px; text-align: center">Ед-цы</th>
				<th style="width: 80px; text-align: center">Цена (руб.)</th>
			</thead>
			<tbody *ngFor="let group of (priceList$ | async)?.groups | groupsWithPrices">
				<tr *ngIf="group.name">
					<td style="padding-left: 0" colspan="3">
						<strong>{{group.name}}</strong>
					</td>
				</tr>
				<tr *ngFor="let price of group.prices">
					<td style="padding-left: 0">{{price.name}}</td>
					<td style="text-align: center; vertical-align:top;">{{getUnitName(price.unitId)}}</td>
					<td style="text-align: center; vertical-align:top;">{{price.value}}</td>
				</tr>
			</tbody>
		</table>
	`,
	styles: [`
		.price-list-view th {
			font-size: 0.7em;
			weight: normal;
		}
		.price-list-view {
			width: 100%;
		}
		tbody {
			padding-top: 5px;
			padding-bottom: 5px;
		}
	`]
})
export class PriceListViewComponent implements OnInit {
	@Input() priceListCode: string;
	priceList$: Observable<PriceList>;

	constructor(private store: Store<AppState>) 
	{ 
	}

	ngOnInit() {
		this.priceList$ = this.store.select(s => s.priceLists)
			.map(pls => _.find(pls, {code: this.priceListCode}))
			.do(pls => _.forEach(pls.groups, g => g.prices = _.sortBy(g.prices, ['name'])));
	}

	getUnitName(unitId: number) {
		let unit = _.find(Units, {id: unitId});
		return unit && unit.name;
	}

	hasPrices(group: PriceGroup) {
		return group.prices && group.prices.length > 0;
	}
}

@Pipe({
	name: 'groupsWithPrices'
})
@Injectable()
export class GroupsWithPrices implements PipeTransform {
	transform(groups: PriceGroup[]) {
		return groups.filter(g => g.prices.length > 0);
	}
}