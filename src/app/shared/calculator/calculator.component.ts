import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { Calculator, CalculateSchema, PriceList, PriceGroup, Price, Units } from '../../../domain';
import { Observable } from 'rxjs/Observable';
import { CalculatorModel, IGroup, IPrice } from './calculatorModel';
import * as _ from 'lodash';


@Component({
	selector: 'calculator',
	templateUrl: 'calculator.component.html',
	styleUrls: ['calculator.component.css']
})
export class CalculatorComponent implements OnInit {
	calculator: Calculator;
	priceList: PriceList;
	totalSum: number;
	schemas: CalculateSchema[];
	currentSchema: CalculateSchema;
	selectedSchema: CalculateSchema;
	form: FormGroup;
	value: string;
	calcModel: CalculatorModel; 

	constructor(
		private store: Store<AppState>
	) {
		this.form = new FormGroup({ 'amount': new FormControl('') })
		this.value = '100';
	 }

	ngOnInit() {
		this.store.select(s => s).subscribe(state => {
			if (state.calculator && state.priceLists) {
				this.calculator = state.calculator;
				this.priceList = _.find(state.priceLists, { _id: this.calculator.priceListId });
				if (this.priceList) {
					this.schemas = this.calculator.schemas.filter(sch => sch.published); 
					this.calcModel = new CalculatorModel(this.priceList, this.calculator);
					this.changeSchema(_.first(this.schemas));
				}
			}
		});
	}

	model() {
		return this.calcModel; 
	}

	valueChanged() {
		this.calculate();
	}

	changeSchema(schema: CalculateSchema) {
		this.currentSchema = schema;
		this.setChecks(); 
		this.calculate();
	}

	onSchemaSelected(name: string) {
		let schema = _.find(this.calculator.schemas, {name: name});
		this.changeSchema(schema);
	}

	private setChecks() {
		if (!this.calcModel) return; 

		this.calcModel.groups.forEach(group => group.prices.forEach(price => price.checked = false));
		if (this.currentSchema) {
			this.calcModel.groups.forEach(group => group.prices.forEach(price => {
				let schemaItem = _.find(this.currentSchema.items, {groupId: group.id, priceId: price.id});
				price.checked = schemaItem != null;
			}));
		}
	}

	calculate() {
		this.totalSum = 0;
		
		if (this.currentSchema) {
			// Сначала считаем по схеме
			this.currentSchema.items.forEach(item => {
				let priceListPrice = this.getPrice(item.groupId, item.priceId);
				let group = _.find(this.calcModel.groups, {id: item.groupId});
				let price = group && _.find(group.prices, {id: item.priceId});

				this.totalSum += this.part(priceListPrice, price, item.quantity)
			});
		}

		// Теперь считаем то что ввел пользователь
		this.calcModel.groups.forEach(group => {
			group.prices.forEach(price => {
				let priceListPrice = this.getPrice(group.id, price.id);

				if (price.checked) {
					let schemaItem = this.currentSchema && _.find(this.currentSchema.items, {priceId: price.id, groupId: group.id});
					if (!schemaItem) {
						this.totalSum += this.part(priceListPrice, price, 1)
					}
				}

			});
		});
		
	}

	private part(priceListPrice: Price, price: IPrice, quantity: number) {
		if (!(price && !price.checked)) {
			let unit = this.getUnit(priceListPrice.unitId);
			if (unit.name == "м2") {
				return quantity * priceListPrice.value * parseInt(this.value || '0');
			} else {
				return priceListPrice.value;
			}
		}
		return 0;
	}

	getPrice(groupId: number, priceId: number) {
		let group = _.find(this.priceList.groups, {id: groupId});
		return group && _.find(group.prices, {id: priceId})
	}

	getUnitName(id: number) {
		let unit = this.getUnit(id);
		return unit && unit.name;
	}

	private getUnit(id: number) {
		if (id == null) return null;
		return _.find(Units, { id: id });
	}

	priceCheckedChange() {
		this.calculate();
	}
}