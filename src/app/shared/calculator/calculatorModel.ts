import { PriceList, Calculator } from '../../../domain';
import * as _ from 'lodash';

export interface IPrice {
	id: number;
	checked: boolean;
	groupId: number;
	name: string;
	unitId: number;
	value: number;
}

export interface IGroup {
	id: number;
	name: string;
	prices: IPrice[];
}

export class CalculatorModel {
	groups: IGroup[];

	constructor(private priceList: PriceList, private calculator: Calculator) {
		this.buildCalculatorModel();
	}

	private buildCalculatorModel() {
		this.groups = [];
		this.calculator.items.forEach(item => {
			let priceListGroup = _.find(this.priceList.groups, {id: item.groupId});
			if (!priceListGroup) {
				console.log('PriceListGroup not found');
				return;
			}
			let priceListPrice = _.find(priceListGroup.prices, {id: item.priceId});
			if (!priceListPrice) {
				console.log('PriceListPrice not found');
			}

			let group = _.find(this.groups, {id: item.groupId});
			if (!group) {
				group = { id: item.groupId, name: priceListGroup.name, prices: [] };
				this.groups.push(group);
			}

			group.prices.push({
				id: item.id,
				groupId: item.groupId,
				name: priceListPrice.name,
				checked: false,
				unitId: priceListPrice.unitId,
				value: priceListPrice.value
			})
		})
	}
}