import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UniversalModule } from 'angular2-universal/browser';  // for AoT we need to manually split universal packages
import { COMPILER_PROVIDERS } from '@angular/compiler';

// Application
import { AppComponent } from './app.component';
import { AdminModule } from './admin/admin.module';
import { SharedModule } from './shared/shared.module';
import { APP_DIRECTIVES, APP_SERVICES } from '.';
import { routing } from './app.routing';

import { StoreModule } from '@ngrx/store';
import { reducer } from './store';

// Dev Tools
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreLogMonitorModule, useLogMonitor } from '@ngrx/store-log-monitor';


@NgModule({
	declarations: [ AppComponent, ...APP_DIRECTIVES ],
	imports: [
		routing,
		SharedModule,
		AdminModule,
		StoreModule.provideStore(reducer)
		// StoreDevtoolsModule.instrumentStore({
		// 	monitor: useLogMonitor({
		// 	visible: true,
		// 	position: 'right'
		// 	})
		// }),
		// StoreLogMonitorModule
	],
	providers: [
		COMPILER_PROVIDERS,
		APP_SERVICES
	]
})
export class AppModule {}

export { AppComponent } from './app.component';