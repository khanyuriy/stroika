import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store';
import { Observable } from 'rxjs/Observable';
import { Renova } from '../../../../domain';
import { Router } from '@angular/router';
import { AppService } from '../../../app.service';

@Component({
	templateUrl: 'renova-list.component.html'
})
export class RenovaListComponent implements OnInit {
	renovas$: Observable<Renova[]>;
	constructor(
		private store: Store<AppState>, 
		private router: Router,
		private service: AppService
	) { }

	ngOnInit() {
		this.renovas$ = this.store.select(s => s.renovas);
	}
	add() {
		this.router.navigate(['/admin_998967644736/renovas', 0])
	}
	edit(renova: Renova) {
		this.router.navigate(['/admin_998967644736/renovas', renova._id]);
	}
	delete(renova: Renova) {
		if (confirm(`Вы действительно хотите удалить "${renova.menuText}" ?'`)){
			this.service.deleteRenova(renova._id);
		}
	}
}