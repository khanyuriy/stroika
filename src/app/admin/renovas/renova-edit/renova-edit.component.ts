import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '../../../store';
import { Subscription } from 'rxjs/Subscription';
import { Renova } from '../../../../domain';
import { AppService } from '../../../app.service';

@Component({
	templateUrl: 'renova-edit.component.html'
})
export class RenovaEditComponent implements OnInit {
	renovaForm: FormGroup;
	renovas: Renova[];
	renova: Renova;
	sub: Subscription;
	_id: any;

	constructor(
		private store: Store<AppState>, 
		private builder: FormBuilder,
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private service: AppService
	) { 
		this.renovaForm = builder.group({
			'code': [''],
			'published': [''],
			'menuText': [''],
			'headerImageUrl': [''],
			'call2Action': [''],
			'text': ['']
		})
	}

	ngOnInit() { 
		this._id = this.activatedRoute.snapshot.params['id'];
		this.sub = this.store.select(s => s.renovas).subscribe(renovas => {
			this.renova = renovas && renovas.find(r => r._id == this._id);
		})
	}
	update() {
		var renova = this.renovaForm.value;
		renova._id = this._id;
		if (this._id == "0") {
			this.service.postRenova(renova);
		} else {
			this.service.putRenova(renova)
		}
	}
	back() {
		this.router.navigate(['/admin_998967644736/renovas']);
	}
	ngOnDestroy() {
		this.sub.unsubscribe();
	}
}