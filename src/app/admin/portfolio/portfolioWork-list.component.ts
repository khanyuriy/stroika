import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { PortfolioWork } from '../../../domain';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { AppService } from '../../app.service';

@Component({
	templateUrl: 'portfolioWork-list.component.html',
	styleUrls: ['portfolioWork-list.component.css']
})
export class PortfolioWorkListComponent implements OnInit {
	private works$: Observable<PortfolioWork[]>;
	constructor(
		private router: Router,
		private store: Store<AppState>,
		private service: AppService
	) { }

	ngOnInit() { 
		this.works$ = this.store.select(s => s.portfolioWorks);
	}

	addWork() {
		this.router.navigate(['/admin_998967644736/portfolio', 0]);
	}
	editWork(work: PortfolioWork) {
		this.router.navigate(['/admin_998967644736/portfolio', work._id]);
	}
	deleteWork(work: PortfolioWork) {
		if (confirm(`Вы действительно хотите удалить работу из портфолио "${work.title}"`)) {
			this.service.deletePortfolioWork(work._id);
		}
	}
}