import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray } from '@angular/forms';
import { AppService } from '../../app.service';
import { TextEditorComponent } from '../textEditor';
import { PortfolioWork, Renova } from '../../../domain';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';


@Component({
	templateUrl: 'portfolioWork-edit.component.html',
	styleUrls: ['portfolioWork-edit.component.css']
})
export class PortfolioWorkEditComponent implements OnInit, OnDestroy {
	form: FormGroup;
	work: PortfolioWork;
	id: string;
	sub: any;
	renovas$: Observable<Renova[]>;

	constructor(
		private builder: FormBuilder,
		private service: AppService,
		private store: Store<AppState>,
		private route: ActivatedRoute,
		private router: Router
	) {
		this.id = route.snapshot.params['id'];
	}

	ngOnInit() {
		this.renovas$ = this.store.select(s => s.renovas);

		this.sub = this.store.select(s => s.portfolioWorks).subscribe(works => {
			this.work = _.find(works, {_id: this.id});
			this.id = this.work && this.work._id || this.id; 
			if (!this.work) { 
				this.work = new PortfolioWork();
				this.work.date = new Date(); 
			}

			if (!this.form) {
				this.form = new FormGroup({
					'code': new FormControl(''),
					'renovaId': new FormControl(''),
					'title': new FormControl(''),
					'annotation': new FormControl(''),
					'text': new FormControl(''),
					'date': new FormControl(''),
					'mainPhoto': new FormControl(''),
					'photos': new FormArray([]),
					'published': new FormControl('')
				});
				this.work.photos.forEach(
					p => this.addPhotoToForm(p)
				);
			}
		});
	}
	addPhoto() {
		this.work.photos.push('');
		this.addPhotoToForm('');
	}
	addPhotoToForm(photoUrl) {
		(<FormArray>this.form.controls['photos']).push(new FormControl());
	}
	deletePhoto(index: number) {
		this.work.photos.splice(index, 1);
		(<FormArray>this.form.controls['photos']).removeAt(index);
	}
	update() {
		let work = this.form.value;
		work._id = this.id;
		if (this.id == '0') {
			this.service.postPortfolioWork(work);
			this.back();
		} else {
			this.service.putPortfolioWork(work);
		}
	}
	back() {
		this.router.navigate(['/admin_998967644736/portfolio']);
	}
	ngOnDestroy() {
		this.sub.unsubscribe();
	}
}