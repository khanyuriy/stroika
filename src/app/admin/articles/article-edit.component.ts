import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AppService } from '../../app.service';
import { TextEditorComponent } from '../textEditor';
import { Article } from '../../../domain';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import * as _ from 'lodash';

@Component({
	templateUrl: 'article-edit.component.html',
	styleUrls: ['article-edit.component.css']
})
export class ArticleEditComponent implements OnInit {
	form: FormGroup;
	article: Article;
	id: string;

	constructor(
		private builder: FormBuilder,
		private service: AppService,
		private store: Store<AppState>,
		private route: ActivatedRoute,
		private router: Router
	) { 
		this.form = builder.group({
			'code': [''],
			'title': [''],
			'annotation': [''],
			'imageUrl': [''],
			'date': [''],
			'published': [''],
			'text': [''],
		});
		this.id = route.snapshot.params['id'];
	}

	ngOnInit() {
		this.store.select(s => s.articles).subscribe(articles => {
			this.article = _.find(articles, {_id: this.id});
		})
	}

	update() {
		let article = this.form.value;
		article._id = this.id;
		if (this.id == '0') {
			this.service.postArticle(article);
			this.router.navigate(['/admin_998967644736/articles']);
		} else {
			this.service.putArticle(article);
		}
	}
	back() {
		this.router.navigate(['/admin_998967644736/articles']);
	}
}