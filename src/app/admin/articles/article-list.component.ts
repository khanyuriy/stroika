import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Article } from '../../../domain';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { AppService } from '../../app.service';

@Component({
	templateUrl: 'article-list.component.html',
	styleUrls: ['article-list.component.css']
})
export class ArticleListComponent implements OnInit {
	private articles$: Observable<Article[]>;
	constructor(
		private router: Router,
		private store: Store<AppState>,
		private service: AppService
	) { }

	ngOnInit() { 
		this.articles$ = this.store.select(s => s.articles);

	}

	addArticle() {
		this.router.navigate(['/admin_998967644736/articles', 0]);
	}
	editArticle(article: Article) {
		this.router.navigate(['/admin_998967644736/articles', article._id]);
	}
	deleteArticle(article: Article) {
		if (confirm(`Вы действительно хотите удалить статью "${article.title}"`)) {
			this.service.deleteArticle(article._id);
		}
	}
}