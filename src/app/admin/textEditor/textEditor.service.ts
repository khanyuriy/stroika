import { OpaqueToken, ElementRef } from '@angular/core';
export const TEXT_EDITOR_SERVICE : OpaqueToken = new OpaqueToken('TextEditorService'); 

export interface TextEditorService {
	init(element: ElementRef, onChange: any);
	writeValue(value);
	destroy();
}

export function provideTextEditorBrowserService() {
	return { provide: TEXT_EDITOR_SERVICE, useValue: new TextEditorBrowserService() }
}

export function provideTextEditorNodeService() {
	return { provide: TEXT_EDITOR_SERVICE, useValue: new TextEditorNodeService() }
}

declare var tinymce: any;

export class TextEditorBrowserService implements TextEditorService {
	value: any;
	init(element: ElementRef, onChange: any) {
		tinymce.init({
			selector: '#'+element.nativeElement.getElementsByTagName('textarea')[0].id,
			height: 650,
			plugins: 'image,textcolor,table',
			toolbar: "styleselect | forecolor backcolor | undo redo | removeformat | bold italic underline |  aligncenter alignjustify  | bullist numlist outdent indent | link | print | fontselect fontsizeselect | image "+
				"| table tablerowprops tablecellprops ",
			menubar: false,
			statusbar: true,
			image_class_list: [{title: 'Указаная ширина', value: ''}, {title: 'На всю ширину страницы', value: 'img-responsive image-wide'}],
			'formats' : {
				'alignleft' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', attributes: {"align":  'left'}},
				'aligncenter' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', attributes: {"align":  'center'}},
				'alignright' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', attributes: {"align":  'right'}},
				'alignfull' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', attributes: {"align":  'justify'}}
			},
			setup: ed => {
				ed.on('change', e => {
					onChange(ed.getContent());
				});
			},
			language_url: '/assets/tinymce-ru.js'
		}).then(() => {
			this.value && tinymce.activeEditor.setContent(this.value);
		});
	}
	writeValue(value) {
		tinymce.activeEditor 
			? tinymce.activeEditor.setContent(value)
			: this.value = value;
	}
	destroy() {
		tinymce.activeEditor.destroy();
	}
}

export class TextEditorNodeService implements TextEditorService {
	init() {}
	writeValue() {}
	destroy() {}
}