import { Directive, OnDestroy, AfterViewInit, Inject, Input,
	ElementRef, Output, EventEmitter, Component, forwardRef} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR,  } from '@angular/forms';
import { TEXT_EDITOR_SERVICE, TextEditorService } from './textEditor.service';

@Component({
	selector: 'text-editor',
	template: `<textarea id="textarea"></textarea>`,
	providers: [
		{
			provide: NG_VALUE_ACCESSOR, 
			useExisting: forwardRef(() => TextEditorComponent),
			multi: true
		}
	]
})
export class TextEditorComponent implements OnDestroy, AfterViewInit, ControlValueAccessor {
	propagateOnChange = (_:any) => {};
	constructor(
		@Inject(TEXT_EDITOR_SERVICE) private service: TextEditorService,
		private element: ElementRef
	) {
	}

	ngOnDestroy() {
		this.service.destroy();
	}

	ngAfterViewInit() {
		this.service.init(this.element, this.propagateOnChange);
	}

	writeValue(value: any) {
		if (value !== undefined) {
			this.service.writeValue(value);
		}
	}

	registerOnChange(fn) {
		this.propagateOnChange = fn;
	}

	registerOnTouched() { }
}