import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { Observable } from 'rxjs/Observable';
import { PriceList } from '../../../domain'
import { AppService } from '../../app.service'

@Component({
	templateUrl: 'pricelist-list.component.html'
})
export class PriceListsComponent implements OnInit {
	priceLists$: Observable<PriceList[]>;
	constructor(
		private store : Store<AppState>,
		private service: AppService
	) { 
		this.priceLists$ = store.select(s => s.priceLists);
	}

	add() {
		let priceList = new PriceList();
		this.service.postPriceList(priceList);
	}

	updatePriceList(priceList: PriceList) {
		this.service.putPriceList(priceList);
	}

	deletePriceList(priceList: PriceList) {
		if (!priceList.groups || 
			priceList.groups.length == 0 ||
			confirm('Вы действительно хотите удалить выбранный прайс-лист ?')) 
		{
			this.service.deletePriceList(priceList._id);
		}
	}

	ngOnInit() { }

}