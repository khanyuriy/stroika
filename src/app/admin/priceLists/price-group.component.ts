import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, FormControl } from '@angular/forms';
import { PriceGroup, Price, Units, DocumentHelper } from '../../../domain';

@Component({
	selector: 'price-group',
	templateUrl: 'price-group.component.html',
	styleUrls: ['price-group.component.css']
})
export class PriceGroupComponent implements OnInit {
	@Input() form: FormGroup;
	@Input() priceGroup: PriceGroup;
	@Input() priceChangeOnly: boolean;
	@Output() ondelete: EventEmitter<any> = new EventEmitter();
	units = Units;
	constructor() {
		console.log('PriceGroupComponent ctor');
	}

	ngOnInit() {
		if (this.priceGroup.prices) {
			this.priceGroup.prices.forEach(p => this.addPriceToForm(p));
		}
	}

	add() {
		let price = new Price();
		price.id = DocumentHelper.getNextId(this.priceGroup.prices);
		this.priceGroup.prices = this.priceGroup.prices || [];
		this.priceGroup.prices.push(price);
		this.addPriceToForm(price);
	}

	deleteGroup() {
		this.ondelete.emit(null);
	}

	delete(index: number) {
		this.priceGroup.prices.splice(index, 1);
		(<FormArray>this.form.controls['prices']).removeAt(index);
	}

	addPriceToForm(price: Price) {
		(<FormArray>this.form.controls['prices']).push(new FormGroup({
			'name': new FormControl(''),
			'unitId': new FormControl(''),
			'value': new FormControl('')
		}));
	}
}