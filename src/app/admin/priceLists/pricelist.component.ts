import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { PriceList, PriceGroup, DocumentHelper } from '../../../domain';
import { FormBuilder, FormGroup, FormControl, FormArray, AbstractControl } from '@angular/forms';

@Component({
	selector: 'price-list',
	templateUrl: 'pricelist.component.html',
	styleUrls: ['pricelist.component.css']
})
export class PriceListComponent implements OnInit {
	@Input() priceList: PriceList;
	@Output() ondelete: EventEmitter<any> = new EventEmitter();
	@Output() onupdate: EventEmitter<PriceList> = new EventEmitter<PriceList>();
	plForm :FormGroup;
	constructor(private builder: FormBuilder) {	}

	ngOnInit() {
		this.plForm = new FormGroup({
			'name': new FormControl(''),
			'code': new FormControl(''),
			'comment': new FormControl(''),
			'priceChangeOnly': new FormControl(''),
			'groups': new FormArray([]),
			'published': new FormControl('')
		})
		if (this.priceList.groups) {
			this.priceList.groups.forEach(g => this.addGroupToForm(g))
		}
	}

	createFormGroup(group: PriceGroup): FormGroup {
		return new FormGroup({
			'name': new FormControl(''),
			'prices': new FormArray([])
		});
	}

	update() {
		this.onupdate.emit(this.priceList);
	}

	add() {
		let group = new PriceGroup();
		group.id = DocumentHelper.getNextId(this.priceList.groups);
		this.priceList.groups = this.priceList.groups || [];
		this.priceList.groups.push(group);
		this.addGroupToForm(group);
	}

	deletePriceList() {
		this.ondelete.emit(null);
	}

	deleteGroup(index) {
		if (
			!this.priceList.groups[index].prices ||
			this.priceList.groups[index].prices.length == 0 || 
			confirm('Вы действительно хотите удалить группу цен ?')) 
		{
			this.priceList.groups.splice(index, 1);
			(<FormArray>this.plForm.controls['groups']).removeAt(index);
		}
	}

	addGroupToForm(g: PriceGroup) {
		(<FormArray>this.plForm.controls['groups']).push(<AbstractControl>this.createFormGroup(g));
	}

}