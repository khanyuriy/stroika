import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';

import { 
	AdminComponent, 
	SettingsComponent,
	ReviewsAdminComponent, 
	ReviewEditComponent, 
	ReviewListComponent,
	RenovasComponent,
	RenovaListComponent,
	RenovaEditComponent,
	PriceListsComponent,
	CalculatorComponent,
	ArticlesComponent,
	ArticleListComponent,
	ArticleEditComponent,
	PortfolioComponent,
	PortfolioWorkListComponent,
	PortfolioWorkEditComponent,
	VideosComponent,
	VideoEditComponent
} from '../admin';

export const routing: ModuleWithProviders =  RouterModule.forChild([
	{
		path: 'admin_998967644736', component: AdminComponent, 
		children: [
			{ path: '', component: SettingsComponent },
			{ 
				path: 'reviews', 
				component: ReviewsAdminComponent, 
				children: [
					{ path: '', component: ReviewListComponent },
					{ path: ':id', component: ReviewEditComponent }
				]
			},
			{ 
				path: 'renovas', 
				component: RenovasComponent, 
				children: [
					{ path: '', component: RenovaListComponent },
					{ path: ':id', component: RenovaEditComponent }
				]
			},
			{
				path: 'articles',
				component: ArticlesComponent,
				children: [
					{ path: '', component: ArticleListComponent},
					{ path: ':id', component: ArticleEditComponent}
				]
			},
			{
				path: 'pricelists',
				component: PriceListsComponent
			},
			{
				path: 'calculator',
				component: CalculatorComponent
			},
			{
				path: 'portfolio',
				component: PortfolioComponent,
				children: [
					{ path: '', component: PortfolioWorkListComponent },
					{ path: ':id', component: PortfolioWorkEditComponent }
				]
			},
			{path: 'vg', component: VideosComponent},
			{path: 'vg/:id', component: VideoEditComponent}
		]
	}
]);