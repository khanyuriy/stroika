import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { routing } from './admin.routing';

import { SharedModule } from '../shared/shared.module';

import { TextEditorComponent } from './textEditor';
import { provideTextEditorBrowserService } from './textEditor';
import { PriceListComponent, PriceGroupComponent, PriceListsComponent } from './priceLists';
import { PortfolioComponent, PortfolioWorkEditComponent, PortfolioWorkListComponent } from './portfolio';
import { AdminComponent } from './admin.component';
import { SettingsComponent } from './settings/settings.component';
import { ArticleEditComponent, ArticleListComponent, ArticlesComponent } from './articles';
import { CalculatorComponent } from './calculator';
import { RenovaEditComponent, RenovaListComponent, RenovasComponent } from './renovas'
import { ReviewEditComponent, ReviewListComponent, ReviewsAdminComponent } from './reviews';
import { VideoEditComponent, VideosComponent } from './videos';

@NgModule({
	imports: [
		SharedModule,
		routing
	],
	declarations: [
		AdminComponent,
		ArticlesComponent, ArticleListComponent, ArticleEditComponent,
		CalculatorComponent,
		PortfolioComponent, PortfolioWorkEditComponent, PortfolioWorkListComponent,
		PriceListsComponent, PriceListComponent, PriceGroupComponent,
		RenovasComponent, RenovaEditComponent, RenovaListComponent,
		ReviewEditComponent, ReviewListComponent, ReviewsAdminComponent,
		SettingsComponent,
		TextEditorComponent,
		VideoEditComponent, VideosComponent
	]
})
export class AdminModule {

}