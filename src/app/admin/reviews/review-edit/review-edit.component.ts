import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from '../../../app.service';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { AppState } from '../../../store/appState';
import { Review } from '../../../../domain';

@Component({
	templateUrl: 'review-edit.component.html'
})
export class ReviewEditComponent implements OnInit, OnDestroy {
	reviewForm: FormGroup;
	reviewId: string;
	review: Review;
	sub: Subscription;

	constructor(
		private builder: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
		private service: AppService,
		private store: Store<AppState>
	) { 
		this.reviewForm = builder.group({
			author: ['', Validators.required],
			comment: ['', Validators.required],
			date: ['', Validators.required],
			photoUrl: [''],
			published: ['']
		});
	}

	ngOnInit() {
		this.reviewId = this.route.snapshot.params['id'];
		this.sub = this.store.select(state => state.reviews).subscribe(reviews => {
			this.review = reviews && reviews.find(r => r._id == this.reviewId);
		})
	}
	update() {
		var review = this.reviewForm.value;
		review._id = this.reviewId;
		if (this.reviewId == "0") {
			this.service.postReview(review);
		} else {
			this.service.putReview(review)
		}
		this.router.navigate(['/admin_998967644736/reviews']);
	}
	back() {
		this.router.navigate(['/admin_998967644736/reviews']);
	}
	ngOnDestroy() {
		this.sub.unsubscribe();
	}
}