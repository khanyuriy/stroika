import { Component, OnInit } from '@angular/core';
import { AppState } from '../../../store/appState';
import { AppService } from '../../../app.service';
import { Store } from '@ngrx/store';
import { Review } from '../../../../domain';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Component({
	templateUrl: 'review-list.component.html',
})
export class ReviewListComponent implements OnInit {
	reviews$: Observable<Review[]>;
	constructor(
		private store: Store<AppState>, 
		private service: AppService,
		private router: Router
	) { }

	ngOnInit() { 
		this.reviews$ =  this.store.select(state => state.reviews);
	}
	add() {
		this.router.navigate(['/admin_998967644736/reviews', 0]);
	}
	edit(review: Review) {
		this.router.navigate(['/admin_998967644736/reviews', review._id]);
	}
	delete(review: Review) {
		if (confirm(`Вы действительно хотите удалить отзыв ${review.author} ?`)){
			this.service.deleteReview(review._id);
		}
	}
}