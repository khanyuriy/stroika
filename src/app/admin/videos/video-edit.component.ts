import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AppService } from '../../app.service';
import { TextEditorComponent } from '../textEditor';
import { Video } from '../../../domain';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';


@Component({
	templateUrl: 'video-edit.component.html',
	styleUrls: ['video-edit.component.css']
})
export class VideoEditComponent implements OnInit, OnDestroy {
	id: string;
	form: FormGroup;
	video: Video;
	sub: any;

	constructor(
		private builder: FormBuilder,
		private service: AppService,
		private store: Store<AppState>,
		private route: ActivatedRoute,
		private router: Router
	) { 
		this.form = this.builder.group({
			'title': [''],
			'date': [''],
			'published': [''],
			'url': [''],
			'annotation': ['']
		});
		this.id = route.snapshot.params['id'];
	}

	ngOnInit() {
		this.sub = this.store.select(s => s.videos).subscribe(videos => {
			this.video = _.find(videos, {_id: this.id});
			if (!this.video) {
				this.video = new Video();
				this.video.date = new Date();
			} 
		})
	}

	ngOnDestroy() {
		this.sub.unsubscribe();
	}

	update() {
		let video = this.form.value;
		video._id = this.id;
		if (video._id == '0') {
			this.service.postVideo(video);
			this.back();
		} else {
			this.service.putVideo(video);
		}
	}

	back() {
		this.router.navigate(['/admin_998967644736/vg']);
	}
}