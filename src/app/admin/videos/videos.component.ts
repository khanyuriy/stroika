import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Video } from '../../../domain';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { AppService } from '../../app.service';

@Component({
	moduleId: __filename,
	templateUrl: 'videos.component.html',
	styleUrls: ['videos.component.css']
})
export class VideosComponent implements OnInit {
	videos$: Observable<Video[]>
	constructor(
		private router: Router,
		private store: Store<AppState>,
		private service: AppService
	) { }

	ngOnInit() { 
		this.videos$ = this.store.select(state => state.videos);
	}

	addVideo() {
		this.router.navigate(['/admin_998967644736/vg', 0]);
	};

	editVideo(video: Video) {
		this.router.navigate(['/admin_998967644736/vg', video._id]);
	}

	deleteVideo(video: Video) {
		if (confirm('Вы действительно хотите удалить это видео ?')) {
			this.service.deleteVideo(video._id);
		}
	}

}