import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Settings } from '../../../domain';
import { AppState } from '../../store/appState';
import { AppService } from '../../app.service';
import { SETTINGS_LOAD, SETTINGS_PUT } from '../../store/reducers/settings.reducer';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

@Component({
	templateUrl: 'settings.component.html'
})
export class SettingsComponent implements OnInit, OnDestroy {
	settingsForm: FormGroup;
	sub: Subscription;
	set: Settings;

	constructor(private store: Store<AppState>, private service: AppService, builder: FormBuilder ) {
		this.settingsForm = builder.group({
			companyName: ['', Validators.required],
			email: ['', Validators.required],
			phone: ['', Validators.required],
			address: ['', Validators.required],
			mapX: [''],
			mapY: [''],
			description: [''],
			headerImageUrl: [''],
			mainPageText: [''],
			mainPageTitle: ['']
		});
	}
	update() {
		this.service.putSettings(this.settingsForm.value);
	}
	ngOnInit() {
		this.sub = this.store.select(state => state.settings).subscribe(set => {
			this.set = set;
		});
	}
	ngOnDestroy() {
		this.sub && this.sub.unsubscribe();
	}
}