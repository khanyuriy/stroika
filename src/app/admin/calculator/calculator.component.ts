import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { Observable } from 'rxjs/Observable';
import { DocumentHelper, Calculator, CalculatorItem, CalculateSchema, CalculateSchemaItem, PriceList, PriceGroup, Price, Units, NestedDocument } from '../../../domain';
import { Router } from '@angular/router';
import { AppService } from '../../app.service';
import { FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';
import { CalculatorModel } from './calculatorModel';
import * as _ from 'lodash';

@Component({
	templateUrl: 'calculator.component.html',
	styleUrls: ['calculator.component.css']
})
export class CalculatorComponent implements OnInit, OnDestroy {
	sub: any;
	priceLists: PriceList[];
	priceList: PriceList;
	calculator: Calculator;
	currentSchemaId: number;
	calcModel: CalculatorModel;
	constructor(
		private store: Store<AppState>,
		private service: AppService
	) {
	}

	ngOnInit() {
		this.sub = Observable.combineLatest(
			this.store.select(s => s.priceLists),
			this.store.select(s => s.calculator)
		).subscribe(([priceLists, calculator]) => {
			if (priceLists && calculator) {
				this.priceLists = priceLists;
				this.calculator = calculator;
				this.priceList = _.find(this.priceLists, {_id: this.calculator.priceListId});
				if (this.priceList) {
					this.createCalcModel();
				}
			}
		});
	}

	priceListIdChange() {
		if (this.calculator) {
			this.priceList = _.find(this.priceLists, {_id: this.calculator.priceListId});
			if (this.priceList) {
				this.createCalcModel();
			}
		}
	}

	addSchema() {
		this.currentSchemaId = this.calcModel.addSchema();
	}

	private createCalcModel() {
		this.calcModel = new CalculatorModel(this.priceList, this.calculator);
		this.currentSchemaId = this.currentSchemaId || this.calcModel.getFirstSchemaId();
	}

	model() {
		return this.calcModel.getSchemaModelById(this.currentSchemaId);
	}

	form() {
		return this.calcModel.getSchemaFormById(this.currentSchemaId);
	}

	getUnitName(id: number) {
		if (id == null) return null;
		return _.find(Units, { id: id }).name;
	}

	update() {
		let calculator = this.calcModel.toCalculator();
		calculator.priceListId = this.priceList._id;

		this.service.putCalculator(calculator);
	}


	ngOnDestroy() {
		this.sub && this.sub.unsubscribe();
	}
}

