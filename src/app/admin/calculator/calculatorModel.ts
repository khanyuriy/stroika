import { DocumentHelper, Calculator, CalculatorItem, CalculateSchema, CalculateSchemaItem, PriceList, PriceGroup, Price, Units, NestedDocument } from '../../../domain';
import { FormGroup, FormArray, FormControl } from '@angular/forms';
import * as _ from 'lodash';

interface IPrice {
	id: number;
	groupId: number;
	name: string;
	unitId: number;
	value: number;
	quantity: number;
}

interface IGroup {
	id: number;
	name: string;
	prices: IPrice[];
}

interface ISchema {
	id: number;
	name: string;
	published: boolean;
	groups: IGroup[];
}

interface IPriceCheck {
	id: number;
	checked: boolean;
}

interface IGroupCheck {
	id: number;
	prices: IPriceCheck[];
	checked: boolean;
}

interface SchemaModels {
	model: ISchema;
	form: FormGroup;
}


export class CalculatorModel {

	groupChecks: IGroupCheck[];
	schemas: SchemaModels[];
	dummyForm: FormGroup;

	constructor(private priceList: PriceList, private calculator: Calculator) {
		this.dummyForm = new FormGroup({});
		this.createChecksModel();
		this.schemas = this.calculator.schemas && this.calculator.schemas.map<SchemaModels>(sch => this.createModels(sch));

		if (!this.schemas || this.schemas.length == 0) {
			this.addSchema();
		}
	}
	getFirstSchemaId() {
		return this.schemas && _.first(this.schemas).model.id;
	}
	getSchemaModelById(id: any) {
		let schema = _.find(this.schemas, sch => sch.model.id == parseInt(id));
		return schema && schema.model;
	}
	getSchemaFormById(id: any) {
		let schema = _.find(this.schemas, sch => sch.model.id == parseInt(id));
		return schema && schema.form || this.dummyForm;
	}
	addSchema() {
		this.schemas = this.schemas || [];
		let nextId = DocumentHelper.getNextId(this.schemas.map(sch => { return {id: sch.model.id}}));
		var schema = new CalculateSchema().deserialize({
			id: nextId,
			name: 'Расчетная схема '+ nextId,
			published: false,
			items: []
		});
		this.schemas.push(this.createModels(schema));
		return nextId;
	}

	createChecksModel() {
		this.groupChecks = [];

		this.priceList.groups.forEach(g => {
			let prices: IPriceCheck[] = [];
			g.prices.forEach(p => {
				prices.push({
					id: p.id,
					checked: this.getCalculatorItem(g.id, p.id) ? true : false
				});
			});

			this.groupChecks.push({
				id: g.id,
				checked: false,
				prices: prices
			});
		})
	}

	createModels(schema: CalculateSchema): SchemaModels {
		//строим модель документа
		let result: SchemaModels = <any>{};
		result.model = <any>{};
		result.model.id = schema.id;
		result.model.name = schema.name;
		result.model.published = schema.published;
		result.model.groups = this.priceList.groups.map(g => {
			return <IGroup>{
				id: g.id,
				name: g.name,
				prices: g.prices.map(p => {
					let calcItem = this.getCalculatorItem(g.id, p.id);
					let schemaItem = this.getSchemaItem(schema.id, g.id, p.id);
					return <IPrice>{
						id: p.id,
						groupId: g.id,
						name: p.name,
						unitId: p.unitId,
						value: p.value,
						quantity: schemaItem ? schemaItem.quantity : 0
					};
				})
			}
		});
		// строим модель формы
		result.form = new FormGroup({
			name: new FormControl(''),
			published: new FormControl(''),
			groups: new FormArray([])
		});
		result.model.groups.forEach(g => {
			let fg = new FormGroup({
				'prices': new FormArray([])
			});
			g.prices.forEach(p => {
				let fgp = new FormGroup({
					'quantity': new FormControl('')
				});
				(<FormArray>fg.controls['prices']).push(fgp);
			});
			(<FormArray>result.form.controls['groups']).push(fg);
		});

		return result;
	}

	private getCalculatorItem(groupId: number, priceId: number) {
		return _.find(this.calculator.items, { groupId: groupId, priceId: priceId });
	}

	private getSchemaItem(id: number, groupId: number, priceId: number) {
		let shema = _.find(this.calculator.schemas, { id: id });
		if (!shema) return null;

		return _.find(shema.items, { groupId: groupId, priceId: priceId });
	}

	toCalculator() : Calculator {
		let calculator = new Calculator();

		this.groupChecks.forEach(ch => ch.prices.forEach(p => {
			if (ch.checked || p.checked) {
				calculator.items.push(new CalculatorItem().deserialize({
					id: DocumentHelper.getNextId(calculator.items),
					groupId: ch.id,
					priceId: p.id
				}));
			}
		}));

		_.forEach(this.schemas, (schema) => { 
			var calculateSchema = new CalculateSchema().deserialize({
				id: schema.model.id,
				name: schema.model.name,
				published: schema.model.published,
				items: []
			})
			calculator.schemas.push(calculateSchema);
			
			_.forEach(schema.model.groups,	g => 
				_.forEach(g.prices, p => {
					if (p.quantity > 0) {
						calculateSchema.items.push(
							new CalculateSchemaItem().deserialize({
								id: DocumentHelper.getNextId(calculateSchema.items),
								groupId: g.id,
								priceId: p.id,
								quantity: p.quantity
							}))
					}
				})
			);
		});

		return calculator;
	}
}