import { Request, Response } from 'express';
import { waterfall } from 'async';
import { DbManager } from './dbManager';
import { Db, Collection } from 'mongodb';
import { PriceList } from '../domain';

const COLLECTION_NAME = 'priceLists';


export class PriceListApi {
	public static findAll(callback: AsyncResultArrayCallback<PriceList, any>) {
		DbManager.findAll(PriceList, COLLECTION_NAME, {}, callback);
	}
	public static get(req: Request, res: Response, next) {
		PriceListApi.findAll((err, result) => {
			err ? next(err)
				: res.send(result);
		})
	}
	public static post(req: Request, res: Response, next) {
		if (!req.body) {
			return next('Пустой документ прайс листа');
		}
		let priceList = new PriceList().deserialize(req.body);
		DbManager.insertOne(PriceList, COLLECTION_NAME, priceList, (err, result) => {
			err ? next(err)
				: res.send({data: {_id: result._id}});
		});
	}
	public static put(req: Request, res: Response, next) {
		if (!req.body) {
			return next('Пустой документ прайс листа');
		}
		let priceList = new PriceList().deserialize(req.body);
		DbManager.updateOne(COLLECTION_NAME, priceList, (err, result) => {
			err ? next(err)
				: res.sendStatus(200);
		});
	}
	public static delete(req: Request, res: Response, next) {
		if (!req.params.id) {
			return next('Не задан id документа');
		}
		let _id = DbManager.toMongoId(req.params.id);
		DbManager.delete(COLLECTION_NAME, {_id: _id}, (err, result) => {
			err ? next(err)
				: res.sendStatus(200);
		})
	}
}