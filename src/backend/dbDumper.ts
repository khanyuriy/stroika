require('events').EventEmitter.prototype._maxListeners = 100;
import Domain = require('../domain');
import path = require('path');
import async = require('async');
import { config } from '../config';
import ChildProcess = require('child_process');
import fs = require('fs');
var rimraf = require('rimraf');
var archiver = require('archiver');


export class DbDumper {
	static dump(destFilePath: string, callback: AsyncResultCallback<any, any>) {
		var cmd = 'mongodump --db '+config.mongoDbName+' --host ' + config.mongoDbHost +
				' --port ' + config.mongoDbPort.toString() + ' --out ' + path.dirname(destFilePath);
		
		if (fs.existsSync(destFilePath)) {
			fs.unlinkSync(destFilePath);
		}
		
		async.series([
			cb => ChildProcess.exec(cmd, null, cb),
			cb => {
				let archive = archiver('zip');
				let output = fs.createWriteStream(destFilePath);

				output.on('close', function () {
					cb(null, null);
				});

				archive.on('error', function(err: Error){
					cb(err, null);
				});

				archive.pipe(output);
				archive.bulk([
					{ expand: true, cwd: path.join(path.dirname(destFilePath), config.mongoDbName), src: ['**/*'], dest: '/' + config.mongoDbName}
				]);
				archive.finalize();
			},
			cb => {
				rimraf(path.join(destFilePath, `../${config.mongoDbName}`), cb)
			}
		], callback);
	}
}