import { Request, Response } from 'express';
import { Db, Collection, ObjectID } from 'mongodb';
import { DbManager } from './dbManager';
import { waterfall } from 'async';
import { Review } from '../domain';
import * as _ from 'lodash';

const COLLECTION_NAME = 'reviews';

export class ReviewApi {
	public static get(req: Request, res: Response, next) {
		ReviewApi.findAll((err, result) => {
			err ? next(err)
				: res.send({data: result});
		})
	}
	public static findAll(callback: AsyncResultCallback<Review[], any>) {
		DbManager.findAll(Review, COLLECTION_NAME, {}, callback);
	}
	// Создание нового документа Review/
	public static post(req: Request, res: Response, next) {
		if (!req.body) {
			next('Пустой документ отзыва');
		}
		var reviewDoc = new Review().deserialize(req.body);
		DbManager.insertOne(Review, COLLECTION_NAME, reviewDoc, (err, result) => {
			err ? next(err)
				: res.status(200).send({data: {_id: result._id}});
		});
	}
	// Обновление существующего документа Review
	public static put(req: Request, res: Response, next) {
		if (!req.body) {
			next('Пустой документ отзыва');
		}
		var reviewDoc = new Review().deserialize(req.body);
		DbManager.updateOne(COLLECTION_NAME, reviewDoc, (err, result) => {
			err ? next(err)
				: res.sendStatus(200);
		});
	}
	public static delete(req: Request, res: Response, next) {
		if (!req.params.id) {
			next('Не указан идентификатор отзыва');
		}
		var _id = DbManager.toMongoId(req.params.id);
		DbManager.delete(COLLECTION_NAME, {_id: _id}, (err, result) => {
			err ? next(err)
				: res.sendStatus(200);
		});
	}
}
