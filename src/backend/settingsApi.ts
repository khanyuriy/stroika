import { Request, Response } from 'express';
import { Db, Collection } from 'mongodb';
import { DbManager } from './dbManager';
import { waterfall } from 'async';
import { Settings } from '../domain';
import * as _ from 'lodash';

const COLLECTION_NAME = 'singles';

export class SettingsApi {
	public static get(req: Request, res: Response) {
		SettingsApi.find((err, settings) => {
			res.send({
				data: settings
			});
		});
	}
	public static find(callback: AsyncResultCallback<Settings, any>){
		DbManager.findOne(Settings, COLLECTION_NAME, {type: 'Settings'}, callback);
	}
	public static put(req: Request, res: Response, next) {
		if (!req.body) {
			next('Пустой документ настроек');
		}

		var settingsDoc = new Settings().deserialize(req.body);
		delete settingsDoc._id;
		DbManager.updateOneEx(COLLECTION_NAME, {type: 'Settings'}, settingsDoc, {upsert: true}, (err, result) => {
			err ? next(err)
				: res.sendStatus(200);
		})
	}
}
