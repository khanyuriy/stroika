import { MongoClient, Db, ObjectID, Collection, ReplaceOneOptions } from 'mongodb';
import { config } from '../config';
import { waterfall } from 'async';
import { Deserializable, Document } from '../domain';

export class DbManager {
	static db: Db;
	static isConnecting: boolean;
	static callQueue: DbCallback[] = [];
	public static get(callback: DbCallback) {
		if (this.db) {
			return callback(null, this.db);
		}

		if (this.isConnecting) {
			this.callQueue.push(callback);
			return;
		}

		this.isConnecting = true;
		this.callQueue.push(callback);
		MongoClient.connect(config.mongodbUri, (err, db) =>{
			this.isConnecting = false;
			if (!err) {
				this.db = db;
			}
			this.callQueue.forEach(cb => cb(err, db));
			this.callQueue = [];
		})

	}
	public static findAll<T extends Document<T>>(type: { new(): T; }, collection: string, query: {}, callback: AsyncResultArrayCallback<T, any>){
		waterfall([
			cb => DbManager.get(cb),
			(db: Db, cb) => db.collection(collection, cb),
			(collection: Collection, cb) => collection.find(query).toArray(cb),
			(result, cb) => cb(null, result.map(r => new type().deserialize(r)))
		], callback)
	}
	public static findOne<T extends Document<T>>(type: {new(): T; }, collection: string, query: {}, callback: AsyncResultCallback<T, any>)
	{
		waterfall([
			cb => DbManager.get(cb),
			(db: Db, cb) => db.collection(collection, cb),
			(collection: Collection, cb) => collection.findOne(query, cb),
			(result, cb) => cb(null, new type().deserialize(result))
		], callback)
	}
	public static insertOne<T extends Document<T>>(type: { new(): T }, collection: string, entity: T, callback: AsyncResultCallback<T, any>) {
		delete entity._id;
		waterfall([
			cb => DbManager.get(cb),
			(db: Db, cb) => db.collection(collection, cb),
			(collection: Collection, cb) => collection.insertOne(entity, cb),
			(result, cb) => {
				var doc = new type().deserialize(result);
				doc._id = result.insertedId;
				cb(null, doc);
			}
		], callback)
	}
	public static updateOne<T extends Document<T>>(collection: string, entity: T, callback: AsyncResultCallback<void, any>) {
		let _id = DbManager.toMongoId(entity._id); 
		delete entity._id;
		DbManager.updateOneEx(collection, {_id: _id}, entity, {}, callback);
	}
	public static updateOneEx<T extends Document<T>>(collection: string, query: {}, entity: T, options: ReplaceOneOptions, callback: AsyncResultCallback<void, any>) {
		waterfall([
			cb => DbManager.get(cb),
			(db: Db, cb) => db.collection(collection, cb),
			(collection: Collection, cb) => collection.updateOne(query, { $set: entity }, options, cb)
		], callback)
	}
	public static delete<T extends Document<T>>(collection: string, query: {}, callback: AsyncResultCallback<void, any>) {
		waterfall([
			cb => DbManager.get(cb),
			(db: Db, cb) => db.collection(collection, cb),
			(collection: Collection, cb) => collection.deleteOne(query, cb),
		], callback);
	}
	public static toMongoId(id: string) {
		return typeof id === 'string'
			? new ObjectID(id)
			: id;
	}
}

interface DbCallback {
	(err: any, db: Db): void;
}