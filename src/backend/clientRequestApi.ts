import { Request, Response } from 'express';
import { ClientRequest, Settings } from '../domain';
import { waterfall } from 'async';
import { SettingsApi } from './settingsApi';
import * as NodeMailer from 'nodemailer';
import { config } from '../config';

export class ClientRequestApi {
	public static post(req: Request, res: Response, next) {
		console.log('ClientRequest post');
		if (!req.body) {
			return next('CilentRequestApi.post. Пустой запрос');
		}
		var r = new ClientRequest().deserialize(req.body);
		waterfall([
			cb => SettingsApi.find(cb),
			(settings: Settings, cb) => {
				console.log(settings);
				if (!settings.email) {
					return cb('Не указан email компании');
				}
				var title = `новая заявка от ${r.phone || r.email}`;
				if (r.fio) { title += ` (${r.fio})`};
				var body = 
					`ФИО: ${r.fio || 'не указан'},\r\n`+
					`Email: ${r.email || 'не указан'},\r\n`+
					`Телефон: ${r.phone || 'не указан'},\r\n`+
					`Комментарий: ${r.comment || 'не указан'}`;
				EmailSender.instance.send(settings.companyName, settings.email, 
					title, body, '', cb);
			}
		], (err, result) => {
			err ? next(err) : res.sendStatus(200);
		})
	}
}

export class EmailSender {
	transport: NodeMailer.Transporter;
	static sender: EmailSender;

	constructor() {
		this.transport = NodeMailer.createTransport(
		{	
			host: 'smtp.yandex.ru',
			port: 465,
			secure: true,
			debug: true,
			auth: {
				user: config.emailUser,
				pass: config.emailPassword
			}
		});
	}
	
	public send(name: string, email: string, subject: string, text: string, html: string, callback: AsyncResultCallback<NodeMailer.SentMessageInfo, any>) {
		
		var options: NodeMailer.SendMailOptions = {
			from: config.emailsFrom,
			to: name+' <'+email+'>',
			subject: subject,
			text: text,
			html: html
		};
		
		this.transport.sendMail(options, callback);
	}

	static get instance() : EmailSender {
		if (!EmailSender.sender) {
			EmailSender.sender = new EmailSender();
		}
		return EmailSender.sender;
	}
}