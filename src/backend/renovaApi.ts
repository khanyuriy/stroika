import { Request, Response } from 'express';
import { waterfall } from 'async';
import { DbManager } from './dbManager';
import { Db, Collection } from 'mongodb';
import { Renova } from '../domain';

const COLLECTION_NAME = 'renovas';

export class RenovaApi {
	public static get(req: Request, res: Response, next) {
		RenovaApi.findAll((err, renovas: Renova[]) => {
			err ? next(err)
				: res.send({data: renovas});
		});
	}
	public static findAll(callback: AsyncResultCallback<Renova[], any>){
		DbManager.findAll(Renova, COLLECTION_NAME, {}, callback); 
	}
	public static getByCode(req: Request, res: Response, next) {
		if (!req.body) {
			return next('Не указан код ремонта');
		}
		let code = req.body;
		DbManager.findOne(Renova, COLLECTION_NAME, {code: code}, (err, renova: Renova) => {
			err ? next(err)
				: res.send({data: renova});
		});
	}
	public static post(req: Request, res: Response, next) {
		if (!req.body) {
			return next('Не задан документ ремонта');
		}
		let renova = new Renova().deserialize(req.body);
		DbManager.insertOne(Renova, COLLECTION_NAME, renova, (err, result)=> {
			err ? next(err)
				: res.send({data: {_id: result._id}});
		});
	}
	public static put(req: Request, res: Response, next) {
		if (!req.body) {
			return next('Не задан документ ремонта');
		}
		let renova = new Renova().deserialize(req.body);
		DbManager.updateOne(COLLECTION_NAME, renova, (err, result) => {
			err ? next(err)
				: res.sendStatus(200);
		})
	}
	public static delete(req: Request, res: Response, next) {
		if (!req.params.id) {
			return next('Не задан _id ремонта');
		}
		let _id = DbManager.toMongoId(req.params.id);
		DbManager.delete(COLLECTION_NAME, {_id: _id}, (err, result) => {
			err ? next(err)
				: res.sendStatus(200);
		});
	}
}