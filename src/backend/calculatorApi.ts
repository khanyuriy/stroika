import { Request, Response } from 'express';
import { waterfall } from 'async';
import { DbManager } from './dbManager';
import { Db, Collection } from 'mongodb';
import { Calculator } from '../domain';

const COLLECTION_NAME = 'singles';


export class CalculatorApi {
	public static find(callback: AsyncResultCallback<Calculator, any>) {
		DbManager.findOne(Calculator, COLLECTION_NAME, {type: 'Calculator'}, callback);
	}
	public static get(req: Request, res: Response, next) {
		CalculatorApi.find((err, result) => {
			err ? next(err)
				: res.send(result);
		})
	}
	public static put(req: Request, res: Response, next) {
		if (!req.body) {
			return next('Пустой документ калькулятора');
		}
		let calc = new Calculator().deserialize(req.body);
		DbManager.updateOneEx(COLLECTION_NAME, {type: 'Calculator'}, calc, {upsert: true}, (err, result) => {
			err ? next(err)
				: res.sendStatus(200);
		});
	}
}