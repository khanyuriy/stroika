import { Request, Response } from 'express';
import { DbManager } from './dbManager';
import { PortfolioWork, Document } from '../domain';

export class SimpleCrudApi<T extends Document<T>> {
	constructor(private type: { new(): T; }, private collectionName: string) {
	}
	public findAll(callback: AsyncResultCallback<T[], any>) {
		DbManager.findAll<T>(this.type, this.collectionName, {}, callback);
	}
	public get(req: Request, res: Response, next) {
		this.findAll((err, result) => {
			err ? next(err)
				: res.send(result);
		});
	}
	public post(req: Request, res: Response, next) {
		if (!req.body) {
			return next('Пустой документ ');
		}
		let entity = new this.type().deserialize(req.body);
		DbManager.insertOne(this.type, this.collectionName, entity, (err, result) => {
			err ? next(err)
				: res.send({data: {_id: result._id}});
		});
	}
	public put(req: Request, res: Response, next) {
		if (!req.body) {
			return next('Пустой документ');
		}
		let entity = new this.type().deserialize(req.body);
		DbManager.updateOne(this.collectionName, entity, (err, result) => {
			err ? next(err)
				: res.sendStatus(200);
		});
	}
	public delete(req: Request, res: Response, next) {
		if (!req.params.id) {
			return next('Не задан id');
		}
		let _id = DbManager.toMongoId(req.params.id);
		DbManager.delete(this.collectionName, {_id: _id}, (err, result) => {
			err ? next(err)
				: res.sendStatus(200);
		})
	}
}