import { Request, Response } from 'express';
import { parallel } from 'async';
import { RenovaApi } from './renovaApi';
import { ReviewApi } from './reviewApi';
import { SettingsApi } from './settingsApi';
import { PriceListApi } from './priceListApi';
import { CalculatorApi } from './calculatorApi';
import { SimpleCrudApi } from './simpleCrudApi';
import { PortfolioWork } from '../domain';
import { articlesApi, portfolioWorksApi, videoApi } from './apis';

export class InitStateApi {
	public static get(req: Request, res: Response, next) {
		parallel([
			cb => SettingsApi.find(cb),
			cb => RenovaApi.findAll(cb),
			cb => ReviewApi.findAll(cb),
			cb => PriceListApi.findAll(cb),
			cb => CalculatorApi.find(cb),
			cb => articlesApi.findAll(cb),
			cb => portfolioWorksApi.findAll(cb),
			cb => videoApi.findAll(cb)
		], (err, result) => {
			err ? next(result) 
				: res.send({data: {
					settings: result[0],
					renovas: result[1],
					reviews: result[2],
					priceLists: result[3],
					calculator: result[4],
					articles: result[5],
					portfolioWorks: result[6],
					videos: result[7]
				}}).end();
		})
	}
}