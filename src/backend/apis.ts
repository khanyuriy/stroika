import { PortfolioWork, Article, Video } from '../domain';
import { SimpleCrudApi } from './simpleCrudApi';

export var articlesApi = new SimpleCrudApi<Article>(Article, 'articles');
export var portfolioWorksApi = new SimpleCrudApi<PortfolioWork>(PortfolioWork, 'portfolioWorks');
export var videoApi = new SimpleCrudApi<Video>(Video, 'videos')