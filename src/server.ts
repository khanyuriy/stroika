// the polyfills must be one of the first things imported in node.js.
// The only modules to be imported higher - node modules with es6-promise 3.x or other Promise polyfill dependency
// (rule of thumb: do it if you have zone.js exception that it has been overwritten)
// if you are including modules that modify Promise, such as NewRelic,, you must include them before polyfills
import 'angular2-universal-polyfills';
import 'ts-helpers';
import './__workaround.node'; // temporary until 2.1.1 things are patched in Core

import * as path from 'path';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as morgan from 'morgan';
import * as compression from 'compression';

// Angular 2
import { enableProdMode } from '@angular/core';
// Angular 2 Universal
import { createEngine } from 'angular2-express-engine';

// App
import { MainModule } from './node.module';
import { DbDumper } from './backend/dbDumper';

// Routes
import { routes } from './server.routes';

// enable prod for faster renders
enableProdMode();

const app = express();
const ROOT = path.join(path.resolve(__dirname, '..'));

// Express View
app.engine('.html', createEngine({
  ngModule: MainModule,
  providers: [
    // use only if you have shared state between users
    // { provide: 'LRU', useFactory: () => new LRU(10) }

    // stateless providers only since it's shared
  ]
}));
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname);
app.set('view engine', 'html');
app.set('json spaces', 2);

app.use(cookieParser('Angular 2 Universal'));
app.use(bodyParser.json());
app.use(compression());

app.use(morgan('dev'));

function cacheControl(req, res, next) {
  // instruct browser to revalidate in 60 seconds
  res.header('Cache-Control', 'max-age=60');
  next();
}
// Serve static files
app.use('/assets', cacheControl, express.static(path.join(__dirname, 'assets'), {maxAge: 30}));
app.use(cacheControl, express.static(path.join(ROOT, 'dist/client'), {index: false}));
app.use(cacheControl, express.static(path.join(ROOT, 'node_modules/bootstrap/dist'), {index: false}));
app.use(cacheControl, express.static(path.join(ROOT, 'node_modules/font-awesome'), {index: false}));
app.use(cacheControl, express.static(path.join(ROOT, 'node_modules/jquery/dist'), {index: false}));
app.use(cacheControl, express.static(path.join(ROOT, 'node_modules/tinymce'), {index: false}));

import { InitStateApi } from './backend/initStateApi';
app.get('/api/initState', InitStateApi.get);

import { SettingsApi } from './backend/settingsApi';
app.get('/api/settings', SettingsApi.get);
app.put('/api/settings', SettingsApi.put);

import { CalculatorApi } from './backend/calculatorApi';
app.get('/api/calculator', CalculatorApi.get);
app.put('/api/calculator', CalculatorApi.put);

import { ReviewApi } from './backend/reviewApi';
app.get('/api/reviews', ReviewApi.get);
app.post('/api/reviews', ReviewApi.post);
app.put('/api/reviews', ReviewApi.put);
app.delete('/api/reviews/:id', ReviewApi.delete);

import { RenovaApi } from './backend/renovaApi';
app.get('/api/renovas', RenovaApi.get);
app.get('/api/renovas/:code', RenovaApi.getByCode);
app.post('/api/renovas', RenovaApi.post);
app.put('/api/renovas', RenovaApi.put);
app.delete('/api/renovas/:id', RenovaApi.delete);

import { PriceListApi } from './backend/priceListApi';
app.get('/api/priceLists', PriceListApi.get);
app.post('/api/priceLists', PriceListApi.post);
app.put('/api/priceLists', PriceListApi.put);
app.delete('/api/priceLists/:id', PriceListApi.delete);

import { articlesApi, portfolioWorksApi, videoApi } from './backend/apis';

app.get('/api/articles', articlesApi.get.bind(articlesApi));
app.post('/api/articles', articlesApi.post.bind(articlesApi));
app.put('/api/articles', articlesApi.put.bind(articlesApi));
app.delete('/api/articles/:id', articlesApi.delete.bind(articlesApi));

app.get('/api/works', portfolioWorksApi.get.bind(portfolioWorksApi));
app.post('/api/works', portfolioWorksApi.post.bind(portfolioWorksApi));
app.put('/api/works', portfolioWorksApi.put.bind(portfolioWorksApi));
app.delete('/api/works/:id', portfolioWorksApi.delete.bind(portfolioWorksApi));

app.get('/api/videos', videoApi.get.bind(videoApi));
app.post('/api/videos', videoApi.post.bind(videoApi));
app.put('/api/videos', videoApi.put.bind(videoApi));
app.delete('/api/videos/:id', videoApi.delete.bind(videoApi));

app.get('/dump', (req, res) => {
	
	let destFilePath = path.join(ROOT, 'dump.zip');
	DbDumper.dump(destFilePath, (err: any) => {
		if (err) {
			return res.status(500).send();
		}
		console.log(destFilePath);
		res.sendFile(destFilePath);
	});
})

import { ClientRequestApi } from './backend/clientRequestApi';
app.post('/api/clientRequest', ClientRequestApi.post);

function ngApp(req, res) {
  res.render('index', {
    req,
    res,
    // time: true, // use this to determine what part of your app is slow only in development
    preboot: false,
    baseUrl: '/',
    requestUrl: req.originalUrl,
    originUrl: `http://localhost:${ app.get('port') }`
  });
}

function ngAdminApp(req, res) {
  res.render('indexAdmin', {
    req,
    res,
    // time: true, // use this to determine what part of your app is slow only in development
    preboot: false,
    baseUrl: '/',
    requestUrl: req.originalUrl,
    originUrl: `http://localhost:${ app.get('port') }`
  });
}


app.get('/admin_998967644736', ngAdminApp);
app.get('/admin_998967644736/*', ngAdminApp);

/**
 * use universal for specific routes
 */
app.get('/', ngApp);
routes.forEach(route => {
  app.get(`/${route}`, ngApp);
  app.get(`/${route}/*`, ngApp);
});

app.get('*', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  var pojo = { status: 404, message: 'No Content' };
  var json = JSON.stringify(pojo, null, 2);
  res.status(404).send(json);
});

// Server
let server = app.listen(app.get('port'), () => {
  console.log(`Listening on: http://localhost:${server.address().port}`);
});