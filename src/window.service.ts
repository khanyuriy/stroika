import { Injectable, OpaqueToken } from '@angular/core';

export const WINDOW_SERVICE: OpaqueToken = new OpaqueToken('WINDOW_SERVICE');

export interface WindowService {
	scrollToTop();
}

export function provideBrowserWindowService() {
	return { provide: WINDOW_SERVICE, useValue: new BrowserWindowService() }
}

export function provideNodeWindowService() {
	return { provide: WINDOW_SERVICE, useValue: new NodeWindowService() }
}

class BrowserWindowService implements WindowService {
	scrollToTop() {
		window.scroll(0, 0);
	}
}

class NodeWindowService implements WindowService {
	scrollToTop() {
	}
}